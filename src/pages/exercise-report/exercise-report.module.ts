import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExerciseReportPage } from './exercise-report';


@NgModule({
  declarations: [
    ExerciseReportPage,
  ],
  imports: [
    IonicPageModule.forChild(ExerciseReportPage)
  ],
})
export class ExerciseReportPageModule { }