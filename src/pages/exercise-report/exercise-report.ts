import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Tabs } from 'ionic-angular';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-exercise-report',
  templateUrl: 'exercise-report.html',
})
export class ExerciseReportPage {
  @ViewChild('myTabs') tabRef: Tabs;
  db: any;                        // LokiJS database
  users: any;                  // our DB's document collection object
  tab1 = 'DailyReportPage';
  tab2 = 'WeeklyReportPage';
  email;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter();
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });    
    this.email = navParams.get('userEmail');
  }
  loki(){
    // this.users = this.db.getCollection("users");
    // console.log(this.users.data);
    // var resultSet = this.users.find( {userEmail: this.email} )[0];
    // console.log(resultSet);
    // for(var i=0;i<this.users.data.length;i++){
    //   if(this.users.data[i].userEmail == resultSet.userEmail){
        let openTab = this.navParams.get('openTab');
        if (openTab) {
          this.tabRef.select(openTab);  
        }
    //   }
    // }
  }
 
}