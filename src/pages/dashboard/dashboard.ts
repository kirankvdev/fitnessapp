import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Tabs } from 'ionic-angular';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  db: any;                        // LokiJS database
  users: any;                  // our DB's document collection object
  
  email;
  today: any;
  activityType = [];
  isWorkoutActivity: boolean;
  isNoWorkoutActivity: boolean;
  isSubmittedWorkout: boolean;

  current;
  max: number = 5;
  stroke: number = 5;
  radius: number = 135;
  semicircle: boolean = false;
  rounded: boolean = true;
  responsive: boolean = true;
  clockwise: boolean = true;
  color: string = '';
  background: string = '#eaeaea';
  duration: number = 5000;
  animation: string = 'easeOutCubic';
  animationDelay: number = 0;
  animations: string[] = [];
  gradient: boolean = false;
  realCurrent: number = 0;
  rate:number;
  DailyPeaceQscoreText:string = '';
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter();
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });    
    this.email = navParams.get('userEmail');
  }
  loki(){
    this.today = new Date(Date.now());
    var weekdays = new Array(7);
    weekdays[0] = "Sunday";
    weekdays[1] = "Monday";
    weekdays[2] = "Tuesday";
    weekdays[3] = "Wednesday";
    weekdays[4] = "Thursday";
    weekdays[5] = "Friday";
    weekdays[6] = "Saturday";
    this.today = weekdays[this.today.getDay()];
    this.users = this.db.getCollection("users");
    var resultSet = this.users.find( {userEmail: this.email} )[0];
      // this.moderateReports = this.db.getCollection("moderateReports");
      for(var i=0;i<this.users.data.length;i++){
        if(this.users.data[i].userEmail == resultSet.userEmail){
          if(this.today == "Monday"){
            var MondayParse = JSON.parse(this.users.data[i].userReports.Monday);
            if(MondayParse.length == 0){
              this.isNoWorkoutActivity = true;
              this.isWorkoutActivity = false;
            }else{
              this.isNoWorkoutActivity = false;
              this.isWorkoutActivity = true;
              for(var j=0;j<MondayParse.length;j++){
                var MondayActivity = {ActivityName:"", TimePerDay: ""};
                MondayActivity["ActivityName"] = MondayParse[j].ActivityName;
                MondayActivity["TimePerDay"] = MondayParse[j].TimePerDay;
                this.activityType.push(MondayActivity);
              }
            }            
          }
          if(this.today == "Tuesday"){
            var TuesdayParse = JSON.parse(this.users.data[i].userReports.Tuesday);
            if(TuesdayParse.length == 0){
              this.isNoWorkoutActivity = true;
              this.isWorkoutActivity = false;
            }else{
              this.isNoWorkoutActivity = false;
              this.isWorkoutActivity = true;
              for(var j=0;j<TuesdayParse.length;j++){
                var TuesdayActivity = {ActivityName:"", TimePerDay: ""};
                TuesdayActivity["ActivityName"] = TuesdayParse[j].ActivityName;
                TuesdayActivity["TimePerDay"] = TuesdayParse[j].TimePerDay;
                this.activityType.push(TuesdayActivity);
              }
            }
          }
          if(this.today == "Wednesday"){
            var WednesdayParse = JSON.parse(this.users.data[i].userReports.Wednesday);
            if(WednesdayParse.length == 0){
              this.isNoWorkoutActivity = true;
              this.isWorkoutActivity = false;
            }else{
              this.isNoWorkoutActivity = false;
              this.isWorkoutActivity = true;
              for(var j=0;j<WednesdayParse.length;j++){
                var WednesdayActivity = {ActivityName:"", TimePerDay: ""};
                WednesdayActivity["ActivityName"] = WednesdayParse[j].ActivityName;
                WednesdayActivity["TimePerDay"] = WednesdayParse[j].TimePerDay;
                this.activityType.push(WednesdayActivity);
              }
            }
          }
          if(this.today == "Thursday"){
            var ThursdayParse = JSON.parse(this.users.data[i].userReports.Thursday);
            if(ThursdayParse.length == 0){
              this.isNoWorkoutActivity = true;
              this.isWorkoutActivity = false;
            }else{
              this.isNoWorkoutActivity = false;
              this.isWorkoutActivity = true;
              for(var j=0;j<ThursdayParse.length;j++){
                var ThursdayActivity = {ActivityName:"", TimePerDay: ""};
                ThursdayActivity["ActivityName"] = ThursdayParse[j].ActivityName;
                ThursdayActivity["TimePerDay"] = ThursdayParse[j].TimePerDay;
                this.activityType.push(ThursdayActivity);
              }
            }
          }
          if(this.today == "Friday"){
            var FridayParse = JSON.parse(this.users.data[i].userReports.Friday);
            if(FridayParse.length == 0){
              this.isNoWorkoutActivity = true;
              this.isWorkoutActivity = false;
            }else{
              this.isNoWorkoutActivity = false;
              this.isWorkoutActivity = true;
              for(var j=0;j<FridayParse.length;j++){
                var FridayActivity = {ActivityName:"", TimePerDay: ""};
                FridayActivity["ActivityName"] = FridayParse[j].ActivityName;
                FridayActivity["TimePerDay"] = FridayParse[j].TimePerDay;
                this.activityType.push(FridayActivity);
              }
            }
          }
          if(this.today == "Saturday"){
            var SaturdayParse = JSON.parse(this.users.data[i].userReports.Saturday);
            if(SaturdayParse.length == 0){
              this.isNoWorkoutActivity = true;
              this.isWorkoutActivity = false;
            }else{
              this.isNoWorkoutActivity = false;
              this.isWorkoutActivity = true;
              for(var j=0;j<SaturdayParse.length;j++){
                var SaturdayActivity = {ActivityName:"", TimePerDay: ""};
                SaturdayActivity["ActivityName"] = SaturdayParse[j].ActivityName;
                SaturdayActivity["TimePerDay"] = SaturdayParse[j].TimePerDay;
                this.activityType.push(SaturdayActivity);
              }
            }
          }
          if(this.today == "Sunday"){
            var SundayParse = JSON.parse(this.users.data[i].userReports.Sunday);
            if(SundayParse.length == 0){
              this.isNoWorkoutActivity = true;
              this.isWorkoutActivity = false;
            }else{
              this.isNoWorkoutActivity = false;
              this.isWorkoutActivity = true;
              for(var j=0;j<SundayParse.length;j++){
                var SundayActivity = {ActivityName:"", TimePerDay: ""};
                SundayActivity["ActivityName"] = SundayParse[j].ActivityName;
                SundayActivity["TimePerDay"] = SundayParse[j].TimePerDay;
                this.activityType.push(SundayActivity);
              }
            }
                        
          }
          if(this.users.data[i].SubmittedWorkout){
            this.isSubmittedWorkout = false;
          }else{
            this.isSubmittedWorkout = true;
          }
        this.current = this.users.data[i].DailyPeaceQscore;
        }                
      }    
  }
  getOverlayStyle() {
    if(this.current > 0){
      this.DailyPeaceQscoreText= "Collabarative Score is " + this.current;
  
      if(this.current <= 1 || this.current <=2 ){
        this.color = 'red';
      }
      if(this.current > 2 || this.current <= 3){ 
        this.color = 'orange';
      }
      if(this.current > 3 || this.current <=4){
        this.color = 'yellow';
      }
      if(this.current > 4 || this.current <=5){
        this.color = 'green';
      }
      let isSemi = this.semicircle;
      let transform = (isSemi ? '' : 'translateY(50%) ') + 'translateX(-50%)';
    
      return {
        'top': isSemi ? 'auto' : '30%',
        'bottom': isSemi ? '5%' : 'auto',
        'left': '50%',
        'transform': transform,
        '-moz-transform': transform,
        '-webkit-transform': transform,
        'font-size': this.radius / 3.5 + 'px', 
        'position': 'absolute',
        'line-height': '1'
      };
    }else{      
      this.DailyPeaceQscoreText = "Please give your workout inputs"
      let isSemi = this.semicircle;
      let transform = (isSemi ? '' : 'translateY(50%) ') + 'translateX(-50%)';
    
      return {
        'top': isSemi ? 'auto' : '65%',
        'bottom': isSemi ? '5%' : 'auto',
        'left': '50%',
        'transform': transform,
        '-moz-transform': transform,
        '-webkit-transform': transform,
        'font-size': this.radius / 3.5 + 'px', 
        'position': 'absolute',
        'line-height': '1'
      };
    }    
  
    
  }
  createWorkout(){
    this.navCtrl.push('WorkoutSuggestionInitialPage',{userEmail: this.email});
  }
  workoutInputPage(){
    this.navCtrl.push('WorkoutUserInputPage',{userEmail: this.email});
  }
 
}