import { Component} from '@angular/core';
import { NavController, MenuController, IonicPage, NavParams, AlertController } from 'ionic-angular';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-workout-user-input',
  templateUrl: 'workout-user-input.html'
})
export class WorkoutUserInputPage {
  db: any;                        // LokiJS database
  users: any;                  // our DB's document collection object
  reports: any;
  dailyReports: any;
  moderateReports: any;
  vigorousReports: any;
  combinationReports: any;
  today: any;
  email: any;
  
  activityType = [];
  timePerDay = ["0","5","10","15","20","25","30","35","40","45","50","55","60","65","70","75","80","85","90"];
  dailyWorkoutInput = [];

  issubmitModerate: boolean;
  issubmitVigorous: boolean;
  issubmitCombination: boolean;
  
  constructor(public navCtrl: NavController, private alertCtrl: AlertController,public menuCtrl: MenuController, public navParams: NavParams) {
    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter(); 
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });     
    this.email = navParams.get('userEmail');
    this.issubmitModerate = navParams.get('issubmitModerate');
    this.issubmitVigorous = navParams.get('issubmitVigorous');
    this.issubmitCombination = navParams.get('issubmitCombination');
  }
  loki(){
    this.today = new Date(Date.now());
    var weekdays = new Array(7);
    weekdays[0] = "Sunday";
    weekdays[1] = "Monday";
    weekdays[2] = "Tuesday";
    weekdays[3] = "Wednesday";
    weekdays[4] = "Thursday";
    weekdays[5] = "Friday";
    weekdays[6] = "Saturday";
    this.today = weekdays[this.today.getDay()];
    this.users = this.db.getCollection("users");
    var resultSet = this.users.find( {userEmail: this.email} )[0];
    console.log(resultSet.userEmail);
    console.log(this.users.data);
      // this.moderateReports = this.db.getCollection("moderateReports");
      for(var i=0;i<this.users.data.length;i++){
        if(this.users.data[i].userEmail == resultSet.userEmail){
          if(this.today == "Monday"){
            var MondayParse = JSON.parse(this.users.data[i].userReports.Monday);
            for(var j=0;j<MondayParse.length;j++){
              var MondayActivity = {ActivityName:""};
              MondayActivity["ActivityName"] = MondayParse[j].ActivityName;
              this.activityType.push(MondayActivity);
            }
          }
          if(this.today == "Tuesday"){
            var TuesdayParse = JSON.parse(this.users.data[i].userReports.Tuesday);
            for(var j=0;j<TuesdayParse.length;j++){
              var TuesdayActivity = {ActivityName:""};
              TuesdayActivity["ActivityName"] = TuesdayParse[j].ActivityName;
              this.activityType.push(TuesdayActivity);
            }
          }
          if(this.today == "Wednesday"){
            var WednesdayParse = JSON.parse(this.users.data[i].userReports.Wednesday);
            for(var j=0;j<WednesdayParse.length;j++){
              var WednesdayActivity = {ActivityName:""};
              WednesdayActivity["ActivityName"] = WednesdayParse[j].ActivityName;
              this.activityType.push(WednesdayActivity);
            }
          }
          if(this.today == "Thursday"){
            var ThursdayParse = JSON.parse(this.users.data[i].userReports.Thursday);
            for(var j=0;j<ThursdayParse.length;j++){
              var ThursdayActivity = {ActivityName:""};
              ThursdayActivity["ActivityName"] = ThursdayParse[j].ActivityName;
              this.activityType.push(ThursdayActivity);
            }
          }
          if(this.today == "Friday"){
            var FridayParse = JSON.parse(this.users.data[i].userReports.Friday);
            for(var j=0;j<FridayParse.length;j++){
              var FridayActivity = {ActivityName:""};
              FridayActivity["ActivityName"] = FridayParse[j].ActivityName;
              this.activityType.push(FridayActivity);
            }
          }
          if(this.today == "Saturday"){
            var SaturdayParse = JSON.parse(this.users.data[i].userReports.Saturday);
            for(var j=0;j<SaturdayParse.length;j++){
              var SaturdayActivity = {ActivityName:""};
              SaturdayActivity["ActivityName"] = SaturdayParse[j].ActivityName;
              this.activityType.push(SaturdayActivity);
            }
          }
          if(this.today == "Sunday"){
            var SundayParse = JSON.parse(this.users.data[i].userReports.Sunday);
            for(var j=0;j<SundayParse.length;j++){
              var SundayActivity = {ActivityName:""};
              SundayActivity["ActivityName"] = SundayParse[j].ActivityName;
              this.activityType.push(SundayActivity);
            }
          }
        }                
      }
    
  }
  selectTimePerDay(selectedValue: any,index: any){    
    this.dailyWorkoutInput = this.insertTimeItem(this.dailyWorkoutInput,index,selectedValue);
  }
  updateAnswer(data,index){
      if(data.checked==true){
        var selectedActivity = {ActivityName:"",TimePerDay:"",IndexVal:""};
        selectedActivity.IndexVal = index;
        selectedActivity.ActivityName = data.ActivityName;
        selectedActivity.TimePerDay = data.TimePerDayValue;
        this.dailyWorkoutInput.push(selectedActivity);
      } else if(data.checked==false){
        for(var i=0;i<this.dailyWorkoutInput.length;i++){
          if(data.ActivityName == this.dailyWorkoutInput[i].ActivityName){ 
            this.dailyWorkoutInput.splice(i,1); 
          }    
        }    
      }
  }
  insertTimeItem(obj,indx,val){
    for(var i=0;i<obj.length;i++){
      if(obj[i].IndexVal==indx){
        obj[i].TimePerDay = val;
      }
    }
    return obj;
  }
  submitUserActivity(){
    var reports = [];
    if(this.dailyWorkoutInput.length > 0){     
      for(var i=0;i<this.dailyWorkoutInput.length;i++){
        var dailyActivity = {ActivityName:"",TimePerDay:""};
        dailyActivity["ActivityName"] = this.dailyWorkoutInput[i].ActivityName;
        dailyActivity["TimePerDay"] = this.dailyWorkoutInput[i].TimePerDay;
        reports.push(dailyActivity);
      }
      // this.dailyReports = this.db.removeCollection("dailyReports");
      // this.dailyReports = this.db.addCollection("dailyReports", { 
      //   indices: ['location']
      // });
      // this.dailyReports.insert(reports);
      var resultSet = this.users.find( {userEmail: this.email} )[0];
      console.log(resultSet.userEmail);
      for(var i=0; i < this.users.data.length;i++){
        if(this.users.data[i].userEmail == resultSet.userEmail){
          this.users.data[i].dailyReports = reports;
        }
      }
      
      this.db.saveDatabase();
      this.navCtrl.push('OutlookPage',{userEmail: this.email});
    }else{
      let alert = this.alertCtrl.create({
        message: 'Please select workout details',
        buttons: ['Ok'] 
      });
      alert.present();
    }
    
  }
}
