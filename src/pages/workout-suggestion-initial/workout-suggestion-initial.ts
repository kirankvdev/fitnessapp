import { Component} from '@angular/core';
import { NavController, IonicPage,NavParams, ViewController  } from 'ionic-angular';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-workout-suggestion-initial',
  templateUrl: 'workout-suggestion-initial.html'
})
export class WorkoutSuggestionInitialPage {
  db: any;                        // LokiJS database
  users: any;                  // our DB's document collection object
  email;
  isHideBackButton: boolean;

  constructor(public navParams: NavParams,public navCtrl: NavController,private viewCtrl: ViewController) {
    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter();
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });
    this.email = navParams.get('userEmail');
    this.isHideBackButton = navParams.get('isHideBackButton');
  }
  loki(){
    this.users = this.db.getCollection("users"); 
    // this.users = this.db.addCollection("users", {
    //   indices: ['location']
    // }); 
  }
  // workout(){
  //   this.navCtrl.push('WorkoutUserInputPage');
  // }
  ionViewWillEnter() {
      this.viewCtrl.showBackButton(false);
  }
  openModerateActivity(){
    this.navCtrl.push('ModeratePage',{userEmail: this.email});
  }
  openVigorousActivity(){
    this.navCtrl.push('VigorousPage',{userEmail: this.email});
  }
  openCombinationActivity(){
    this.navCtrl.push('CombinationPage',{userEmail: this.email}); 
  }
}
