import { Component } from '@angular/core';
import { NavController, IonicPage, NavParams, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-personal-details',
  templateUrl: 'personal-details.html'
})
export class PersonalDetailsPage {
  db: any;                        // LokiJS database
  users: any;                  // our DB's document collection object
  userdata;

  PersonalFormData: FormGroup;
  email;
  gender;
  height;
  dob;
  age:any;
  birthdate:any;
  today: any;
  isMedicalOthers;
  isOriginOthers;
  heightValue;
  weightValue;
  originValue;
  medicalValue;
  originOthers;
  medicalOthers;
  workTypevalue;
  heights: any = [];
  weights: any = [];
  genders = [
    "Male",
    "Female",
    "Others"
  ]
  origins = [
    "African American","Asian American","Native American","British Asian","Hispanic/latino","White/Caucasian",
    "Arabic/Middle Eastern",
    "South East Asian/Malay/Thai/Chinese",
    "Indian/Pakistani/Bangladesh",
    "2 or more races",
    "Others"];
  workTypes = [
    "stay at home parent/guardian",
    "part time professional",
    "Self employed/entrepreneur",
    "Full time professional",
    "unemployed",
    "Retired",
    "Student"];
  medicalConditions = [
    "None",
    "Asthma",
    "High/Low blood pressure",
    "High Blood sugar/Diabetic",
    "Low sugar",
    "Heart condition",
    "Severe allergy",
    "Others"];
  constructor(public navCtrl: NavController,public navParams: NavParams,private alertCtrl: AlertController,private storage: Storage,public formBuilder: FormBuilder) {
    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter();
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });
    this.medicalValue = 'None';
    this.email = navParams.get('userEmail');
    this.PersonalFormData = this.formBuilder.group({
        gender: new FormControl('', [Validators.required]),
        birthdate: new FormControl('', [Validators.required]),
        height: new FormControl('', [Validators.required]),
        weight: new FormControl('', [Validators.required]),
        origin: new FormControl('', [Validators.required]),
        workType: new FormControl('', [Validators.required]) 
    });
    for(var i=147;i<196;i++){
      this.heights.push(i);
    }
    for(var j=41;j<146;j++){
      this.weights.push(j);
    }
  }
  loki(){
    this.users = this.db.getCollection("users"); 
    
    // this.users = this.db.addCollection("users", {
    //   indices: ['location']
    // }); 
  }
  // ionViewDidLoad(){
  //   Promise.all([]).then(()=>this.storageGet());
  // }
  // storageGet(){
  //   this.storage.get('userdata').then((data)=>{this.userdata=data;});
  // }
  selectHeight(selectedValue: any){
    this.heightValue = selectedValue;
  }
  selectWeight(selectedValue: any){
    this.weightValue = selectedValue;
  }
  selectOrigin(selectedValue: any){
    if (selectedValue == "Others"){
      this.isOriginOthers=true;
      // this.originValue = this.originOthers; 
    } else {
      this.isOriginOthers=false;
      this.originValue = selectedValue;
    }
    this.originValue = selectedValue;
  }
  selectworkType(selectedValue: any){
    this.workTypevalue = selectedValue;
  }
  selectMedical(selectedValue: any){
    if (selectedValue == "Others"){
      this.isMedicalOthers=true;
      // this.medicalValue = this.medicalOthers;
    } else {
      this.isMedicalOthers=false;
      this.medicalValue = selectedValue;
    }
  }
  getToday(): string {
    return new Date().toISOString().split('T')[0]
 }
  computeBMI() {
    var resultSet = this.users.find( {userEmail: this.email} )[0];
    // this.birthdate = new Date(this.birthdate);
    // this.today = new Date(Date.now());
    // var timeDiff = Math.abs(this.today - this.birthdate);
    // this.age = Math.floor((timeDiff / (1000 * 3600 * 24))/365);

    this.heightValue = this.heightValue;
    this.weightValue = this.weightValue; 
    
    var BMI = this.weightValue/(this.heightValue/100*this.heightValue/100)
    BMI = Math.round(BMI* 100) / 100;
      
    resultSet["gender"] = this.gender;
    resultSet["DOB"] = this.birthdate;
    resultSet["userHeight"] = this.heightValue;
    resultSet["userWeight"] = this.weightValue;
    resultSet["userOrigin"] = this.originValue;
    resultSet["userWorkType"] = this.workTypevalue;
    resultSet["userMedicalConditions"] = this.medicalValue;
    resultSet["BMI"] = BMI;

    this.users.gender = resultSet.gender;
    this.users.DOB = resultSet.DOB;
    this.users.userHeight = resultSet.userHeight; 
    this.users.userWeight = resultSet.userWeight;
    this.users.userOrigin = resultSet.userOrigin;
    this.users.userWorkType = resultSet.userWorkType;
    this.users.userMedicalConditions = resultSet.userMedicalConditions;
    this.users.BMI = resultSet.BMI;

    this.db.saveDatabase();
    if(this.medicalValue == "None"){
      if (BMI < 18.5){
        let alert = this.alertCtrl.create({
          title: 'Your BMI is '+ BMI,
          message: 'You are Underweight',
          buttons: ['Ok']
        });
        alert.present();
        this.navCtrl.push('LoginPage');
      }
          
      else if (BMI >= 18.5 && BMI <= 24.9){         
        let alert = this.alertCtrl.create({
          title: 'Your BMI is '+ BMI,
          message: 'You are Normal',
          buttons: ['Ok']
        });
        alert.present();
        resultSet["isAlreadyLoggedIn"] = true;       
        this.users.isAlreadyLoggedIn = resultSet.isAlreadyLoggedIn;
        this.db.saveDatabase();
        this.navCtrl.push('AreaOfInterestPage',{userEmail: this.email});
      }
      else if (BMI >= 25 && BMI <= 29.9){
        resultSet["isAlreadyLoggedIn"] = true;       
        this.users.isAlreadyLoggedIn = resultSet.isAlreadyLoggedIn;
        this.db.saveDatabase();
        let alert = this.alertCtrl.create({
          title: 'Your BMI is '+ BMI,
          message: 'You are Overweight',
          buttons: ['Ok']
        });
        alert.present();
        this.navCtrl.push('AreaOfInterestPage',{userEmail: this.email});
      }
      else if (BMI > 30){
        resultSet["isAlreadyLoggedIn"] = true;       
        this.users.isAlreadyLoggedIn = resultSet.isAlreadyLoggedIn;
        this.db.saveDatabase();
        let alert = this.alertCtrl.create({
          title: 'Your BMI is '+ BMI,
          message: "You are Obese",
          buttons: ['Ok']
        });
        alert.present();
        this.navCtrl.push('AreaOfInterestPage',{userEmail: this.email});
      }
    }else{
      let alert = this.alertCtrl.create({
        title: 'Your BMI is '+ BMI,
        message: "Since you've Medical conditions we recommend you to consult a doctor",
        buttons: ['Ok']
      });
      alert.present();
      this.navCtrl.push('LoginPage'); 
    }
      
  }
  
}
