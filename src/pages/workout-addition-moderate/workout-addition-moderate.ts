import { Component} from '@angular/core';
import { NavController, MenuController, IonicPage, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-workout-addition-moderate',
  templateUrl: 'workout-addition-moderate.html'
})
export class WorkoutAdditionModeratePage {
  ActivityAdd;
  TypeAdd;
  TimeAdd;
  DaysAdd
  activityType=[ 
    { ActivityName: "walking fast", Type: "Moderate"}, 
    { ActivityName: "water aerobics", Type: "Moderate"}, 
    { ActivityName: "riding a bike on level ground or with few hills", Type: "Moderate"},
    { ActivityName: "doubles tennis", Type: "Moderate"},
    { ActivityName: "pushing a lawn mower", Type: "Moderate"},
    { ActivityName: "hiking", Type: "Moderate"},
    { ActivityName: "skateboarding", Type: "Moderate"},    
    { ActivityName: "rollerblading", Type: "Moderate"},    
    { ActivityName: "volleyball", Type: "Moderate"},    
    { ActivityName: "basketball", Type: "Moderate"},        
    { ActivityName: "going to the gym", Type: "Moderate"}    
  ];
  timePerDay = ["5","10","15","20","25","30","35","40","45","50","55","60","65","70","75","80","85","90"];
  dayPerWeek = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
  
  constructor(public navCtrl: NavController, 
    public menuCtrl: MenuController,
    public navParams: NavParams) {

   
  }
  submitAdditionActivity(){
    console.log("New Activity ",this.ActivityAdd);
    console.log("New Time ",this.TimeAdd);
    console.log("New Days ",this.DaysAdd);
    this.navCtrl.push('WorkoutPlanPage',{ActivityAdd: this.ActivityAdd,TimeAdd: this.TimeAdd,DaysAdd: this.DaysAdd});
  }
  cancelAdditionActivity(){
    this.navCtrl.push("WorkoutPlanPage");
  }
}
