import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WorkoutAdditionModeratePage } from './workout-addition-moderate';


@NgModule({
  declarations: [
    WorkoutAdditionModeratePage,
  ],
  imports: [
    IonicPageModule.forChild(WorkoutAdditionModeratePage)
  ],
})
export class WorkoutAdditionModeratePageModule { }