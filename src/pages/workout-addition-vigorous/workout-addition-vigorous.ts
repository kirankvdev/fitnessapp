import { Component} from '@angular/core';
import { NavController, MenuController, IonicPage, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-workout-addition-vigorous',
  templateUrl: 'workout-addition-vigorous.html'
})
export class WorkoutAdditionVigorousPage { 
  ActivityAdd;
  TypeAdd;
  TimeAdd;
  DaysAdd
  activityType=[ 
    { ActivityName: "jogging or running", Type: "Vigorous"}, 
    { ActivityName: "swimming fast", Type: "Vigorous"}, 
    { ActivityName: "riding a bike fast or on hills", Type: "Vigorous"},
    { ActivityName: "singles tennis", Type: "Vigorous"},
    { ActivityName: "football", Type: "Vigorous"},
    { ActivityName: "rugby", Type: "Vigorous"},
    { ActivityName: "skipping rope", Type: "Vigorous"},    
    { ActivityName: "hockey", Type: "Vigorous"},    
    { ActivityName: "aerobics", Type: "Vigorous"},    
    { ActivityName: "gymnastics", Type: "Vigorous"},        
    { ActivityName: "martial arts", Type: "Vigorous"}    
  ];
  timePerDay = ["5","10","15","20","25","30","35","40","45","50","55","60","65","70","75","80","85","90"];
  dayPerWeek = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
  
  constructor(public navCtrl: NavController, 
    public menuCtrl: MenuController,
    public navParams: NavParams) {

   
  }
  submitAdditionActivity(){
    console.log("New Activity ",this.ActivityAdd);
    console.log("New Time ",this.TimeAdd);
    console.log("New Days ",this.DaysAdd);
    this.navCtrl.push('WorkoutPlanPage',{ActivityAdd: this.ActivityAdd,TimeAdd: this.TimeAdd,DaysAdd: this.DaysAdd});
  }
  cancelAdditionActivity(){
    this.navCtrl.push("WorkoutPlanPage");
  }
}
