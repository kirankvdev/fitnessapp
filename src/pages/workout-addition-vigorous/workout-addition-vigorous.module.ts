import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WorkoutAdditionVigorousPage } from './workout-addition-vigorous';


@NgModule({
  declarations: [
    WorkoutAdditionVigorousPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkoutAdditionVigorousPage)
  ],
})
export class WorkoutAdditionVigorousPageModule { }