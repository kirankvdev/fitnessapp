import { Component} from '@angular/core';
import { NavController, MenuController, IonicPage, NavParams, AlertController } from 'ionic-angular';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-workout-plan',
  templateUrl: 'workout-plan.html'
})
export class WorkoutPlanPage {
  db: any;                        // LokiJS database
  users: any;                  // our DB's document collection object
  reports: any;
  moderateReports: any;
  vigorousReports: any;
  combinationReports: any;

  email;

  isModerateWork: boolean;
  isVigorousWork: boolean;
  isCombination: boolean;

  isModerateFlag:boolean;
  isVigorousFlag:boolean;
  isCombinationFlag: boolean;

  iseditModerate: boolean;
  iseditVigorous: boolean;
  iseditCombination: boolean;

  ActivityAdd = "";
  TimeAdd = "";
  DaysAdd = "";

  isMondayActivity: boolean;
  isTuesdayActivity: boolean;
  isWednesdayActivity: boolean;
  isThursdayActivity: boolean;
  isFridayActivity: boolean;
  isSaturdayActivity: boolean;
  isSundayActivity: boolean;

  issubmitModerate: boolean;
  issubmitVigorous: boolean;
  issubmitCombination: boolean;

  Monday =  [];
  Tuesday = [];
  Wednesday = [];
  Thursday = [];
  Friday = [];
  Saturday = [];
  Sunday = [];

  constructor(public navCtrl: NavController,public menuCtrl: MenuController,public navParams: NavParams,private alertCtrl: AlertController) {
    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter(); 
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });
    this.email = navParams.get('userEmail');
    this.isModerateFlag = navParams.get('isModerateFlag');
    this.isVigorousFlag = navParams.get('isVigorousFlag');
    this.isCombinationFlag = navParams.get('isCombinationFlag');

    // var add = {ActivityName:"",Type:"Moderate",TimePerDay:"",DaysPerWeek:""};
    // var addtemp = {ActivityName:"",Type:"Moderate",TimePerDay:""};
    // this.ActivityAdd = navParams.get('ActivityAdd');
    // this.TimeAdd = navParams.get('TimeAdd');
    // this.DaysAdd = navParams.get('DaysAdd');
    // add["ActivityName"] = this.ActivityAdd;
    // add["TimePerDay"] = this.TimeAdd;     
    // add["DaysPerWeek"] = this.DaysAdd;
    // addtemp["ActivityName"] = this.ActivityAdd;
    // addtemp["TimePerDay"] = this.TimeAdd;

    // if(add.DaysPerWeek != undefined){
    //   console.log(add.DaysPerWeek.length);
    //   for(var i=0;i<add.DaysPerWeek.length;i++){
    //     console.log(add.DaysPerWeek[i]);
    //     if(add.DaysPerWeek[i] == "Monday"){
    //       this.Monday.push(addtemp);         
    //     }
    //     if(add.DaysPerWeek[i] == "Tuesday"){
    //       this.Tuesday.push(addtemp);         
    //     }
    //     if(add.DaysPerWeek[i] == "Wednesday"){
    //       this.Wednesday.push(addtemp);         
    //     }
    //     if(add.DaysPerWeek[i] == "Thursday"){
    //       this.Thursday.push(addtemp);         
    //     }
    //     if(add.DaysPerWeek[i] == "Friday"){
    //       this.Friday.push(addtemp);        
    //     }
    //     if(add.DaysPerWeek[i] == "Saturday"){
    //       this.Saturday.push(addtemp);         
    //     }
    //     if(add.DaysPerWeek[i] == "Sunday"){
    //       this.Sunday.push(addtemp);        
    //     }        
    //   } 
    // }
  }
  loki(){
    this.users = this.db.getCollection("users");  
      var resultSet = this.users.find( {userEmail: this.email} )[0];      
      resultSet["userFirstTime"] = false;       
        this.users.userFirstTime = resultSet.userFirstTime;
      for(var i=0;i<this.users.data.length;i++){ 
        if(this.users.data[i].userEmail == resultSet.userEmail){
          var MondayParse = JSON.parse(this.users.data[i].userReports.Monday);
          var TuesdayParse = JSON.parse(this.users.data[i].userReports.Tuesday);
          var WednesdayParse = JSON.parse(this.users.data[i].userReports.Wednesday);
          var ThursdayParse = JSON.parse(this.users.data[i].userReports.Thursday);
          var FridayParse = JSON.parse(this.users.data[i].userReports.Friday);
          var SaturdayParse = JSON.parse(this.users.data[i].userReports.Saturday);
          var SundayParse = JSON.parse(this.users.data[i].userReports.Sunday);
          for(var j=0;j<MondayParse.length;j++)
          {
            var MondayActivity = {ActivityName:"",Type:"",TimePerDay:""};       
            MondayActivity["ActivityName"] = MondayParse[j].ActivityName;
            MondayActivity["Type"] = MondayParse[j].Type;
            MondayActivity["TimePerDay"] = MondayParse[j].TimePerDay; 
            this.Monday.push(MondayActivity);
            this.isMondayActivity = true;   
          }      
          for(var j=0;j<TuesdayParse.length;j++){      
            var TuesdayActivity = {ActivityName:"",Type:"",TimePerDay:""}; 
            TuesdayActivity["ActivityName"] = TuesdayParse[j].ActivityName;
            TuesdayActivity["Type"] = TuesdayParse[j].Type;
            TuesdayActivity["TimePerDay"] = TuesdayParse[j].TimePerDay;        
            this.Tuesday.push(TuesdayActivity);
            this.isTuesdayActivity = true;
          }      
          for(var j=0;j<WednesdayParse.length;j++){        
            var WednesdayActivity = {ActivityName:"",Type:"",TimePerDay:""}; 
            WednesdayActivity["ActivityName"] = WednesdayParse[j].ActivityName;
            WednesdayActivity["Type"] = WednesdayParse[j].Type;
            WednesdayActivity["TimePerDay"] = WednesdayParse[j].TimePerDay;        
            this.Wednesday.push(WednesdayActivity);
            this.isWednesdayActivity = true;
          }      
          for(var j=0;j<ThursdayParse.length;j++){        
            var ThursdayActivity = {ActivityName:"",Type:"",TimePerDay:""}; 
            ThursdayActivity["ActivityName"] = ThursdayParse[j].ActivityName;
            ThursdayActivity["Type"] = ThursdayParse[j].Type;
            ThursdayActivity["TimePerDay"] = ThursdayParse[j].TimePerDay;        
            this.Thursday.push(ThursdayActivity);
            this.isThursdayActivity = true;
          }      
          for(var j=0;j<FridayParse.length;j++){        
            var FridayActivity = {ActivityName:"",Type:"",TimePerDay:""}; 
            FridayActivity["ActivityName"] = FridayParse[j].ActivityName;
            FridayActivity["Type"] = FridayParse[j].Type;
            FridayActivity["TimePerDay"] = FridayParse[j].TimePerDay;        
            this.Friday.push(FridayActivity);
            this.isFridayActivity = true;
          }      
          for(var j=0;j<SaturdayParse.length;j++){        
            var SaturdayActivity = {ActivityName:"",Type:"",TimePerDay:""}; 
            SaturdayActivity["ActivityName"] = SaturdayParse[j].ActivityName;
            SaturdayActivity["Type"] = SaturdayParse[j].Type;
            SaturdayActivity["TimePerDay"] = SaturdayParse[j].TimePerDay;        
            this.Saturday.push(SaturdayActivity);
            this.isSaturdayActivity = true;
          }      
          for(var j=0;j<SundayParse.length;j++){        
            var SundayActivity = {ActivityName:"",Type:"",TimePerDay:""}; 
            SundayActivity["ActivityName"] = SundayParse[j].ActivityName;
            SundayActivity["Type"] = SundayParse[j].Type;
            SundayActivity["TimePerDay"] = SundayParse[j].TimePerDay;        
            this.Sunday.push(SundayActivity);
            this.isSundayActivity = true;
          }     
        }
         
      }
    this.db.saveDatabase();
    console.log(this.users.data); 
  }
  // addItem(){
  //   if(this.isModerateFlag){
  //     this.navCtrl.push("ModeratePage",{iseditModerate: true});
  //   }
  //   if(this.isVigorousFlag){
  //     this.navCtrl.push("VigorousPage",{iseditVigorous: true}); 
  //   }
  //   if(this.isCombinationFlag){
  //     this.navCtrl.push("CombinationPage",{iseditCombination: true}); 
  //   }
  // }
  reportPage(){
    let alert = this.alertCtrl.create({
      title: 'Success',
      message: 'You need to login again to view your workout',
      buttons: ['Ok']
    });
    alert.present();
      this.navCtrl.push("LoginPage",{userEmail: this.email});
  } 
}
