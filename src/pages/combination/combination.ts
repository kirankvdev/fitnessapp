import { Component} from '@angular/core';
import { NavController, IonicPage, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-combination',
  templateUrl: 'combination.html'
})
export class CombinationPage {
  db: any;                        // LokiJS database
  users: any;                  // our DB's document collection object
  reports: any;
  combinationReports: any;

  email;	
  newindusCombination = [];
  activityTypeValue;
  TimePerDayValue;
  DayPerWeekValue;
  isTimeSpentSelected: boolean;
  isCombinationFlag:boolean; 
  iseditCombination:boolean;
  
  activityType=[ 
    { ActivityName: "walking fast", Type: "Moderate"}, 
    { ActivityName: "water aerobics", Type: "Moderate"}, 
    { ActivityName: "riding a bike on level ground or with few hills", Type: "Moderate"},
    { ActivityName: "doubles tennis", Type: "Moderate"},
    { ActivityName: "pushing a lawn mower", Type: "Moderate"},
    { ActivityName: "hiking", Type: "Moderate"},
    { ActivityName: "skateboarding", Type: "Moderate"},    
    { ActivityName: "rollerblading", Type: "Moderate"},    
    { ActivityName: "volleyball", Type: "Moderate"},    
    { ActivityName: "basketball", Type: "Moderate"},        
    { ActivityName: "going to the gym", Type: "Moderate"},
    { ActivityName: "jogging or running", Type: "Vigorous"}, 
    { ActivityName: "swimming fast", Type: "Vigorous"}, 
    { ActivityName: "riding a bike fast or on hills", Type: "Vigorous"},
    { ActivityName: "singles tennis", Type: "Vigorous"},
    { ActivityName: "football", Type: "Vigorous"},
    { ActivityName: "rugby", Type: "Vigorous"},
    { ActivityName: "skipping rope", Type: "Vigorous"},    
    { ActivityName: "hockey", Type: "Vigorous"},    
    { ActivityName: "aerobics", Type: "Vigorous"},    
    { ActivityName: "gymnastics", Type: "Vigorous"},        
    { ActivityName: "martial arts", Type: "Vigorous"}   
  ];
  timePerDay = ["5","10","15","20","25","30","35","40","45","50","55","60","65","70","75","80","85","90"];
  dayPerWeek = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
  
  constructor(public navCtrl: NavController,private alertCtrl: AlertController,private storage: Storage,public navParams: NavParams) {
    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter();
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });
    this.email = navParams.get('userEmail');
    // this.iseditCombination = navParams.get('iseditCombination');
  }
  loki(){
    this.users = this.db.getCollection("users");
    console.log(this.users);
  }
  selectTimePerDay(selectedValue: any,index: any){
    
    var selectedObjectCount = this.newindusCombination.length;
    this.newindusCombination = this.insertTimeItem(this.newindusCombination,index,selectedValue);
  }
  selectDayPerWeek(selectedValue: any,index: any){
    var selectedObjectCount = this.newindusCombination.length;
    this.newindusCombination = this.insertDayItem(this.newindusCombination,index,selectedValue);
  }
  updateAnswer(data,index){
      if(data.checked==true){
        var selectedActivity = {ActivityName:"",TimePerDay:"",DayPerWeek:"",IndexVal:"",Type:""};
        selectedActivity.IndexVal = index;
        selectedActivity.ActivityName = data.ActivityName;
        selectedActivity.TimePerDay = data.TimePerDayValue;
        selectedActivity.DayPerWeek = data.DayPerWeekValue;
        selectedActivity.Type = data.Type;
       this.newindusCombination.push(selectedActivity); 
      } else if(data.checked==false){
        for(var i=0;i<this.newindusCombination.length;i++){
          if(data.ActivityName == this.newindusCombination[i].ActivityName){
            this.newindusCombination.splice(i,1); 
          }    
        }    
      }
  }
  
  insertTimeItem(obj,indx,val){
    for(var i=0;i<obj.length;i++){
      if(obj[i].IndexVal==indx){
        obj[i].TimePerDay = val;
      }
    }
    return obj;
  }

  insertDayItem(obj,indx,val){
    for(var i=0;i<obj.length;i++){
      if(obj[i].IndexVal==indx){
        obj[i].DayPerWeek = val;
      }
    }
    return obj;
  }

  
  openVigorousActivity(){    
    var reports = {Monday:"",Tuesday: "",Wednesday: "",Thursday: "",Friday: "",Saturday:"",Sunday:""};
    var Monday = [];
    var Tuesday = [];
    var Wednesday = [];
    var Thursday = [];
    var Friday = [];
    var Saturday = [];
    var Sunday = [];
    if(this.newindusCombination.length > 0){
      for(var i=0;i<this.newindusCombination.length;i++){
          for(var j=0;j<this.newindusCombination[i].DayPerWeek.length;j++){
            if(this.newindusCombination[i].DayPerWeek[j] == "Monday")
            {       
              var MondayActivity = {ActivityName:"",Type:"",TimePerDay:""};
              MondayActivity["ActivityName"] = this.newindusCombination[i].ActivityName;
              MondayActivity["Type"] = this.newindusCombination[i].Type;
              MondayActivity["TimePerDay"] = this.newindusCombination[i].TimePerDay;
              Monday.push(MondayActivity);
            }
            if(this.newindusCombination[i].DayPerWeek[j] == "Tuesday") 
            {       
              var TuesdayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              TuesdayActivity["ActivityName"] = this.newindusCombination[i].ActivityName;
              TuesdayActivity["Type"] = this.newindusCombination[i].Type;
              TuesdayActivity["TimePerDay"] = this.newindusCombination[i].TimePerDay; 
              Tuesday.push(TuesdayActivity); 
            }
            if(this.newindusCombination[i].DayPerWeek[j] == "Wednesday")
            {       
              var WednesdayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              WednesdayActivity["ActivityName"] = this.newindusCombination[i].ActivityName;
              WednesdayActivity["Type"] = this.newindusCombination[i].Type;
              WednesdayActivity["TimePerDay"] = this.newindusCombination[i].TimePerDay;
              Wednesday.push(WednesdayActivity);          
            }
            if(this.newindusCombination[i].DayPerWeek[j] == "Thursday")
            {       
              var ThursdayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              ThursdayActivity["ActivityName"] = this.newindusCombination[i].ActivityName;
              ThursdayActivity["Type"] = this.newindusCombination[i].Type;
              ThursdayActivity["TimePerDay"] = this.newindusCombination[i].TimePerDay;
              Thursday.push(ThursdayActivity);         
            }
            if(this.newindusCombination[i].DayPerWeek[j] == "Friday")
            {       
              var FridayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              FridayActivity["ActivityName"] = this.newindusCombination[i].ActivityName;
              FridayActivity["Type"] = this.newindusCombination[i].Type;
              FridayActivity["TimePerDay"] = this.newindusCombination[i].TimePerDay;
              Friday.push(FridayActivity);          
            }
            if(this.newindusCombination[i].DayPerWeek[j] == "Saturday")
            {       
              var SaturdayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              SaturdayActivity["ActivityName"] = this.newindusCombination[i].ActivityName;
              SaturdayActivity["Type"] = this.newindusCombination[i].Type;
              SaturdayActivity["TimePerDay"] = this.newindusCombination[i].TimePerDay;
              Saturday.push(SaturdayActivity);          
            }
            if(this.newindusCombination[i].DayPerWeek[j] == "Sunday")
            {       
              var SundayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              SundayActivity["ActivityName"] = this.newindusCombination[i].ActivityName;
              SundayActivity["Type"] = this.newindusCombination[i].Type;
              SundayActivity["TimePerDay"] = this.newindusCombination[i].TimePerDay;
              Sunday.push(SundayActivity);          
            }
          }
        
      }     
      reports["Monday"] = JSON.stringify(Monday);
      reports["Tuesday"] = JSON.stringify(Tuesday);
      reports["Wednesday"] = JSON.stringify(Wednesday);
      reports["Thursday"] = JSON.stringify(Thursday);
      reports["Friday"] = JSON.stringify(Friday);
      reports["Saturday"] = JSON.stringify(Saturday);
      reports["Sunday"] = JSON.stringify(Sunday);
      // console.log(reports);

        // this.combinationReports = this.db.removeCollection("combinationReports");
        // this.combinationReports = this.db.addCollection("combinationReports");
        // this.combinationReports.insert(reports); 
       var resultSet = this.users.find( {userEmail: this.email} )[0];
          console.log(resultSet.userEmail);
          for(var i=0; i < this.users.data.length;i++){
            if(this.users.data[i].userEmail == resultSet.userEmail){
              this.users.data[i].userReports = reports;
            }
          }
          
          this.db.saveDatabase();
        
        this.navCtrl.push('WorkoutPlanPage',{userEmail: this.email});
  }else{
    let alert = this.alertCtrl.create({
      message: 'Please select the workout plan',
      buttons: ['Ok'] 
    });
    alert.present();
  }
  console.log(this.users.data); 
  }
}
