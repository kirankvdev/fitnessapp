import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-daily-report',
  templateUrl: 'daily-report.html',
})

export class DailyReportPage {
  db: any;                        // LokiJS database
  users: any;                  // our DB's document collection object
  dailyReports: any;
  moderateReports: any;
  vigorousReports: any;
  combinationReports: any;
  today: any;

  medalCounter: any;
  isBronze: boolean;
  isSilver: boolean;
  isGold: boolean;
  dailyMedalCount;
  FinalMedalCount;

  email;
  current: number;
  max: number = 3;
  stroke: number = 5;
  radius: number = 135;
  semicircle: boolean = false;
  rounded: boolean = true;
  responsive: boolean = true;
  clockwise: boolean = true;
  color: string = '';
  background: string = '#eaeaea'; 
  duration: number = 5000;
  animation: string = 'easeOutCubic';
  animationDelay: number = 0;
  animations: string[] = [];
  gradient: boolean = false;
  realCurrent: number = 0;
  rate:number;
  status: string ='';
  constructor(public navCtrl: NavController, public navParams: NavParams,private storage: Storage) {
    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter();
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });    
    // this.email = navParams.get('userEmail');
    storage.get('userdata').then((val) => {
      console.log('Your email id is', val.userEmail);
      this.email = val.userEmail;
    });
    
  }
  loki(){
    this.today = new Date(Date.now());
    var weekdays = new Array(7);
    weekdays[0] = "Sunday";
    weekdays[1] = "Monday";
    weekdays[2] = "Tuesday";
    weekdays[3] = "Wednesday";
    weekdays[4] = "Thursday";
    weekdays[5] = "Friday";
    weekdays[6] = "Saturday";
    this.today = weekdays[this.today.getDay()];
    this.users = this.db.getCollection("users");
    // this.moderateReports = this.db.getCollection("moderateReports");
    // this.vigorousReports = this.db.getCollection("vigorousReports");
    // this.combinationReports = this.db.getCollection("combinationReports");
    // this.dailyReports = this.db.getCollection("dailyReports");
    console.log(this.users.data);
    var TotalMedal = {Monday: "",Tuesday: "",Wednesday: "",Thursday: "",Friday: "",Saturday: "",Sunday: ""}
      var resultSet = this.users.find( {userEmail: this.email} )[0];
      for(var i=0;i<this.users.data.length;i++){
        if(this.users.data[i].userEmail == resultSet.userEmail){
          
          if(this.today == "Monday"){          
            var ActivityCounter = 0;
            var TimeCounter = 0;
            var MondayParse = JSON.parse(this.users.data[i].userReports.Monday);
            for(var j=0;j<MondayParse.length;j++){
              var TargetActivity = {ActivityName:"",TimePerDay:""};
              TargetActivity["ActivityName"] = MondayParse[j].ActivityName;
              TargetActivity["TimePerDay"] = MondayParse[j].TimePerDay;
              var ActualActivity = {ActivityName:"",TimePerDay:""};
              ActualActivity["ActivityName"] = this.users.data[i].dailyReports[j].ActivityName;
              ActualActivity["TimePerDay"] = this.users.data[i].dailyReports[j].TimePerDay;
              if(TargetActivity["ActivityName"] == ActualActivity["ActivityName"]){
                var TargetTime = parseInt(TargetActivity["TimePerDay"]);
                var ActualTime = parseInt(ActualActivity["TimePerDay"]);

                if(ActualTime == 0){
                  console.log("not done"); 
                }else{
                  if(ActualTime > 0 && ActualTime < TargetTime){
                    console.log("Almost There");
                    ActivityCounter = ActivityCounter + 1;
                  }
                  if(ActualTime >= TargetTime){
                    console.log("Superb!!!");
                    ActivityCounter = ActivityCounter + 1; 
                    TimeCounter = TimeCounter + 1;                
                  }
                }                            
              }            
            }
            if(ActivityCounter == 0 && TimeCounter == 0){
              this.current = 0;
            }else if(ActivityCounter > 0 && ActivityCounter < MondayParse.length)
              {
                if(TimeCounter == 0){
                  this.current = 1;
                  this.medalCounter = 1;
                }
                if(TimeCounter > 0 && TimeCounter < MondayParse.length){
                  this.current = 2;
                  this.medalCounter = 2;
                }
              }else if(ActivityCounter == MondayParse.length)
              {
                if(TimeCounter == 0){
                  this.current = 2;
                  this.medalCounter = 2;
                }
                if(TimeCounter > 0 && TimeCounter < MondayParse.length){
                  this.current = 2;
                  this.medalCounter = 2;
                }
                if(TimeCounter == MondayParse.length){
                  if(ActivityCounter == MondayParse.length && TimeCounter == MondayParse.length){
                    this.current = 3;
                    this.medalCounter = 3;
                  }
                }
              } 
              if(this.medalCounter == 1){
                this.isBronze = true;
                this.dailyMedalCount = 1;
              } else if(this.medalCounter > 1 && this.medalCounter == 2 && this.medalCounter < 3){
                this.isBronze = true;
                this.isSilver = true;
                this.dailyMedalCount = 2;
              }else if(this.medalCounter > 1 && this.medalCounter > 2 && this.medalCounter == 3){
                this.isBronze = true;
                this.isSilver = true;
                this.isGold = true;
                this.dailyMedalCount = 3;
              }
              TotalMedal["Monday"] = this.dailyMedalCount;
          }
          if(this.today == "Tuesday"){          
            var ActivityCounter = 0;
            var TimeCounter = 0;
            var TuesdayParse = JSON.parse(this.users.data[i].userReports.Tuesday);
            for(var j=0;j<TuesdayParse.length;j++){
              var TargetActivity = {ActivityName:"",TimePerDay:""};
              TargetActivity["ActivityName"] = TuesdayParse[j].ActivityName;
              TargetActivity["TimePerDay"] = TuesdayParse[j].TimePerDay;
              var ActualActivity = {ActivityName:"",TimePerDay:""};
              ActualActivity["ActivityName"] = this.users.data[i].dailyReports[j].ActivityName;
              ActualActivity["TimePerDay"] = this.users.data[i].dailyReports[j].TimePerDay;
              if(TargetActivity["ActivityName"] == ActualActivity["ActivityName"]){
                var TargetTime = parseInt(TargetActivity["TimePerDay"]);
                var ActualTime = parseInt(ActualActivity["TimePerDay"]);

                if(ActualTime == 0){
                  console.log("not done"); 
                }else{
                  if(ActualTime > 0 && ActualTime < TargetTime){
                    console.log("Almost There");
                    ActivityCounter = ActivityCounter + 1;
                  }
                  if(ActualTime >= TargetTime){
                    console.log("Superb!!!");
                    ActivityCounter = ActivityCounter + 1; 
                    TimeCounter = TimeCounter + 1;                
                  }
                }                            
              }            
            }
            if(ActivityCounter == 0 && TimeCounter == 0){
              this.current = 0;
            }else if(ActivityCounter > 0 && ActivityCounter < TuesdayParse.length)
              {
                if(TimeCounter == 0){
                  this.current = 1;
                  this.medalCounter = 1;
                }
                if(TimeCounter > 0 && TimeCounter < TuesdayParse.length){
                  this.current = 2;
                  this.medalCounter = 2;
                }
              }else if(ActivityCounter == TuesdayParse.length)
              {
                if(TimeCounter == 0){
                  this.current = 2;
                  this.medalCounter = 2;
                }
                if(TimeCounter > 0 && TimeCounter < TuesdayParse.length){
                  this.current = 2;
                  this.medalCounter = 2;
                }
                if(TimeCounter == TuesdayParse.length){
                  if(ActivityCounter == TuesdayParse.length && TimeCounter == TuesdayParse.length){
                    this.current = 3;
                    this.medalCounter = 3;
                  }
                }
              } 
              if(this.medalCounter == 1){
                this.isBronze = true;
                this.dailyMedalCount = 1;
              } else if(this.medalCounter > 1 && this.medalCounter == 2 && this.medalCounter < 3){
                this.isBronze = true;
                this.isSilver = true;
                this.dailyMedalCount = 2;
              }else if(this.medalCounter > 1 && this.medalCounter > 2 && this.medalCounter == 3){
                this.isBronze = true;
                this.isSilver = true;
                this.isGold = true;
                this.dailyMedalCount = 3;
              }
              TotalMedal["Tuesday"] = this.dailyMedalCount;
          }
          if(this.today == "Wednesday"){          
            var ActivityCounter = 0;
            var TimeCounter = 0;
            var WednesdayParse = JSON.parse(this.users.data[i].userReports.Wednesday);
            for(var j=0;j<WednesdayParse.length;j++){
              var TargetActivity = {ActivityName:"",TimePerDay:""};
              TargetActivity["ActivityName"] = WednesdayParse[j].ActivityName;
              TargetActivity["TimePerDay"] = WednesdayParse[j].TimePerDay;
              var ActualActivity = {ActivityName:"",TimePerDay:""};
              ActualActivity["ActivityName"] = this.users.data[i].dailyReports[j].ActivityName;
              ActualActivity["TimePerDay"] = this.users.data[i].dailyReports[j].TimePerDay;
              if(TargetActivity["ActivityName"] == ActualActivity["ActivityName"]){
                var TargetTime = parseInt(TargetActivity["TimePerDay"]);
                var ActualTime = parseInt(ActualActivity["TimePerDay"]);

                if(ActualTime == 0){
                  console.log("not done"); 
                }else{
                  if(ActualTime > 0 && ActualTime < TargetTime){
                    console.log("Almost There");
                    ActivityCounter = ActivityCounter + 1;
                  }
                  if(ActualTime >= TargetTime){
                    console.log("Superb!!!");
                    ActivityCounter = ActivityCounter + 1; 
                    TimeCounter = TimeCounter + 1;                
                  }
                }                            
              }            
            }
            if(ActivityCounter == 0 && TimeCounter == 0){
              this.current = 0;
            }else if(ActivityCounter > 0 && ActivityCounter < WednesdayParse.length)
              {
                if(TimeCounter == 0){
                  this.current = 1;
                  this.medalCounter = 1;
                }
                if(TimeCounter > 0 && TimeCounter < WednesdayParse.length){
                  this.current = 2;
                  this.medalCounter = 2;
                }
              }else if(ActivityCounter == WednesdayParse.length)
              {
                if(TimeCounter == 0){
                  this.current = 2;
                  this.medalCounter = 2;
                }
                if(TimeCounter > 0 && TimeCounter < WednesdayParse.length){
                  this.current = 2;
                  this.medalCounter = 2;
                }
                if(TimeCounter == WednesdayParse.length){
                  if(ActivityCounter == WednesdayParse.length && TimeCounter == WednesdayParse.length){
                    this.current = 3;
                    this.medalCounter = 3;
                  }
                }
              } 
              if(this.medalCounter == 1){
                this.isBronze = true;
                this.dailyMedalCount = 1;
              } else if(this.medalCounter > 1 && this.medalCounter == 2 && this.medalCounter < 3){
                this.isBronze = true;
                this.isSilver = true;
                this.dailyMedalCount = 2;
              }else if(this.medalCounter > 1 && this.medalCounter > 2 && this.medalCounter == 3){
                this.isBronze = true;
                this.isSilver = true;
                this.isGold = true;
                this.dailyMedalCount = 3;
              }
              TotalMedal["Wednesday"] = this.dailyMedalCount;
          }
          if(this.today == "Thursday"){          
            var ActivityCounter = 0;
            var TimeCounter = 0;
            var ThursdayParse = JSON.parse(this.users.data[i].userReports.Thursday);
            for(var j=0;j<ThursdayParse.length;j++){
              var TargetActivity = {ActivityName:"",TimePerDay:""};
              TargetActivity["ActivityName"] = ThursdayParse[j].ActivityName;
              TargetActivity["TimePerDay"] = ThursdayParse[j].TimePerDay;
              var ActualActivity = {ActivityName:"",TimePerDay:""};
              ActualActivity["ActivityName"] = this.users.data[i].dailyReports[j].ActivityName;
              ActualActivity["TimePerDay"] = this.users.data[i].dailyReports[j].TimePerDay;
              if(TargetActivity["ActivityName"] == ActualActivity["ActivityName"]){
                var TargetTime = parseInt(TargetActivity["TimePerDay"]);
                var ActualTime = parseInt(ActualActivity["TimePerDay"]);

                if(ActualTime == 0){
                  console.log("not done"); 
                }else{
                  if(ActualTime > 0 && ActualTime < TargetTime){
                    console.log("Almost There");
                    ActivityCounter = ActivityCounter + 1;
                  }
                  if(ActualTime >= TargetTime){
                    console.log("Superb!!!");
                    ActivityCounter = ActivityCounter + 1; 
                    TimeCounter = TimeCounter + 1;                
                  }
                }                            
              }            
            }
            if(ActivityCounter == 0 && TimeCounter == 0){
              this.current = 0;
            }else if(ActivityCounter > 0 && ActivityCounter < ThursdayParse.length)
              {
                if(TimeCounter == 0){
                  this.current = 1;
                  this.medalCounter = 1;
                }
                if(TimeCounter > 0 && TimeCounter < ThursdayParse.length){
                  this.current = 2;
                  this.medalCounter = 2;
                }
              }else if(ActivityCounter == ThursdayParse.length)
              {
                if(TimeCounter == 0){
                  this.current = 2;
                  this.medalCounter = 2;
                }
                if(TimeCounter > 0 && TimeCounter < ThursdayParse.length){
                  this.current = 2;
                  this.medalCounter = 2;
                }
                if(TimeCounter == ThursdayParse.length){
                  if(ActivityCounter == ThursdayParse.length && TimeCounter == ThursdayParse.length){
                    this.current = 3;
                    this.medalCounter = 3;
                  }
                }
              } 
              if(this.medalCounter == 1){
                this.isBronze = true;
                this.dailyMedalCount = 1;
              } else if(this.medalCounter > 1 && this.medalCounter == 2 && this.medalCounter < 3){
                this.isBronze = true;
                this.isSilver = true;
                this.dailyMedalCount = 2;
              }else if(this.medalCounter > 1 && this.medalCounter > 2 && this.medalCounter == 3){
                this.isBronze = true;
                this.isSilver = true;
                this.isGold = true;
                this.dailyMedalCount = 3;
              }
              TotalMedal["Thursday"] = this.dailyMedalCount;
          }
          if(this.today == "Friday"){          
            var ActivityCounter = 0;
            var TimeCounter = 0;
            var FridayParse = JSON.parse(this.users.data[i].userReports.Friday);
            for(var j=0;j<FridayParse.length;j++){
              var TargetActivity = {ActivityName:"",TimePerDay:""};
              TargetActivity["ActivityName"] = FridayParse[j].ActivityName;
              TargetActivity["TimePerDay"] = FridayParse[j].TimePerDay;
              var ActualActivity = {ActivityName:"",TimePerDay:""};
              ActualActivity["ActivityName"] = this.users.data[i].dailyReports[j].ActivityName;
              ActualActivity["TimePerDay"] = this.users.data[i].dailyReports[j].TimePerDay;
              if(TargetActivity["ActivityName"] == ActualActivity["ActivityName"]){
                var TargetTime = parseInt(TargetActivity["TimePerDay"]);
                var ActualTime = parseInt(ActualActivity["TimePerDay"]);

                if(ActualTime == 0){
                  console.log("not done"); 
                }else{
                  if(ActualTime > 0 && ActualTime < TargetTime){
                    console.log("Almost There");
                    ActivityCounter = ActivityCounter + 1;
                  }
                  if(ActualTime >= TargetTime){
                    console.log("Superb!!!");
                    ActivityCounter = ActivityCounter + 1; 
                    TimeCounter = TimeCounter + 1;                
                  }
                }                            
              }            
            }
            if(ActivityCounter == 0 && TimeCounter == 0){
              this.current = 0;
            }else if(ActivityCounter > 0 && ActivityCounter < FridayParse.length)
              {
                if(TimeCounter == 0){
                  this.current = 1;
                  this.medalCounter = 1;
                }
                if(TimeCounter > 0 && TimeCounter < FridayParse.length){
                  this.current = 2;
                  this.medalCounter = 2;
                }
              }else if(ActivityCounter == FridayParse.length)
              {
                if(TimeCounter == 0){
                  this.current = 2;
                  this.medalCounter = 2;
                }
                if(TimeCounter > 0 && TimeCounter < FridayParse.length){
                  this.current = 2;
                  this.medalCounter = 2;
                }
                if(TimeCounter == FridayParse.length){
                  if(ActivityCounter == FridayParse.length && TimeCounter == FridayParse.length){
                    this.current = 3;
                    this.medalCounter = 3;
                  }
                }
              } 
              if(this.medalCounter == 1){
                this.isBronze = true;
                this.dailyMedalCount = 1;
              } else if(this.medalCounter > 1 && this.medalCounter == 2 && this.medalCounter < 3){
                this.isBronze = true;
                this.isSilver = true;
                this.dailyMedalCount = 2;
              }else if(this.medalCounter > 1 && this.medalCounter > 2 && this.medalCounter == 3){
                this.isBronze = true;
                this.isSilver = true;
                this.isGold = true;
                this.dailyMedalCount = 3;
              }
              TotalMedal["Friday"] = this.dailyMedalCount;
          }
          if(this.today == "Saturday"){          
            var ActivityCounter = 0;
            var TimeCounter = 0;
            var SaturdayParse = JSON.parse(this.users.data[i].userReports.Saturday);
            for(var j=0;j<SaturdayParse.length;j++){
              var TargetActivity = {ActivityName:"",TimePerDay:""};
              TargetActivity["ActivityName"] = SaturdayParse[j].ActivityName;
              TargetActivity["TimePerDay"] = SaturdayParse[j].TimePerDay;
              var ActualActivity = {ActivityName:"",TimePerDay:""};
              ActualActivity["ActivityName"] = this.users.data[i].dailyReports[j].ActivityName;
              ActualActivity["TimePerDay"] = this.users.data[i].dailyReports[j].TimePerDay;
              if(TargetActivity["ActivityName"] == ActualActivity["ActivityName"]){
                var TargetTime = parseInt(TargetActivity["TimePerDay"]);
                var ActualTime = parseInt(ActualActivity["TimePerDay"]);

                if(ActualTime == 0){
                  console.log("not done"); 
                }else{
                  if(ActualTime > 0 && ActualTime < TargetTime){
                    console.log("Almost There");
                    ActivityCounter = ActivityCounter + 1;
                  }
                  if(ActualTime >= TargetTime){
                    console.log("Superb!!!");
                    ActivityCounter = ActivityCounter + 1; 
                    TimeCounter = TimeCounter + 1;                
                  }
                }                            
              }            
            }
            if(ActivityCounter == 0 && TimeCounter == 0){
              this.current = 0;
            }else if(ActivityCounter > 0 && ActivityCounter < SaturdayParse.length)
              {
                if(TimeCounter == 0){
                  this.current = 1;
                  this.medalCounter = 1;
                }
                if(TimeCounter > 0 && TimeCounter < SaturdayParse.length){
                  this.current = 2;
                  this.medalCounter = 2;
                }
              }else if(ActivityCounter == SaturdayParse.length)
              {
                if(TimeCounter == 0){
                  this.current = 2;
                  this.medalCounter = 2;
                }
                if(TimeCounter > 0 && TimeCounter < SaturdayParse.length){
                  this.current = 2;
                  this.medalCounter = 2;
                }
                if(TimeCounter == SaturdayParse.length){
                  if(ActivityCounter == SaturdayParse.length && TimeCounter == SaturdayParse.length){
                    this.current = 3;
                    this.medalCounter = 3;
                  }
                }
              } 
              if(this.medalCounter == 1){
                this.isBronze = true;
                this.dailyMedalCount = 1;
              } else if(this.medalCounter > 1 && this.medalCounter == 2 && this.medalCounter < 3){
                this.isBronze = true;
                this.isSilver = true;
                this.dailyMedalCount = 2;
              }else if(this.medalCounter > 1 && this.medalCounter > 2 && this.medalCounter == 3){
                this.isBronze = true;
                this.isSilver = true;
                this.isGold = true;
                this.dailyMedalCount = 3;
              }
              TotalMedal["Saturday"] = this.dailyMedalCount;
          }
          if(this.today == "Sunday"){          
            var ActivityCounter = 0;
            var TimeCounter = 0;
            var SundayParse = JSON.parse(this.users.data[i].userReports.Sunday);
            for(var j=0;j<SundayParse.length;j++){
              var TargetActivity = {ActivityName:"",TimePerDay:""};
              TargetActivity["ActivityName"] = SundayParse[j].ActivityName;
              TargetActivity["TimePerDay"] = SundayParse[j].TimePerDay;
              var ActualActivity = {ActivityName:"",TimePerDay:""};
              ActualActivity["ActivityName"] = this.users.data[i].dailyReports[j].ActivityName;
              ActualActivity["TimePerDay"] = this.users.data[i].dailyReports[j].TimePerDay;
              if(TargetActivity["ActivityName"] == ActualActivity["ActivityName"]){
                var TargetTime = parseInt(TargetActivity["TimePerDay"]);
                var ActualTime = parseInt(ActualActivity["TimePerDay"]);

                if(ActualTime == 0){
                  console.log("not done"); 
                }else{
                  if(ActualTime > 0 && ActualTime < TargetTime){
                    console.log("Almost There");
                    ActivityCounter = ActivityCounter + 1;
                  }
                  if(ActualTime >= TargetTime){
                    console.log("Superb!!!");
                    ActivityCounter = ActivityCounter + 1; 
                    TimeCounter = TimeCounter + 1;                
                  }
                }                            
              }            
            }
            if(ActivityCounter == 0 && TimeCounter == 0){
              this.current = 0;
            }else if(ActivityCounter > 0 && ActivityCounter < SundayParse.length)
              {
                if(TimeCounter == 0){
                  this.current = 1;
                  this.medalCounter = 1;
                }
                if(TimeCounter > 0 && TimeCounter < SundayParse.length){
                  this.current = 2;
                  this.medalCounter = 2;
                }
              }else if(ActivityCounter == SundayParse.length)
              {
                if(TimeCounter == 0){
                  this.current = 2;
                  this.medalCounter = 2;
                }
                if(TimeCounter > 0 && TimeCounter < SundayParse.length){
                  this.current = 2;
                  this.medalCounter = 2;
                }
                if(TimeCounter == SundayParse.length){
                  if(ActivityCounter == SundayParse.length && TimeCounter == SundayParse.length){
                    this.current = 3;
                    this.medalCounter = 3;
                  }
                }
              } 
              if(this.medalCounter == 1){
                this.isBronze = true;
                this.dailyMedalCount = 1;
              } else if(this.medalCounter > 1 && this.medalCounter == 2 && this.medalCounter < 3){
                this.isBronze = true;
                this.isSilver = true;
                this.dailyMedalCount = 2;
              }else if(this.medalCounter > 1 && this.medalCounter > 2 && this.medalCounter == 3){
                this.isBronze = true;
                this.isSilver = true;
                this.isGold = true;
                this.dailyMedalCount = 3;
              }
              TotalMedal["Sunday"] = this.dailyMedalCount;
          }
        }    
             
      }      
    
    
    if(this.current == 0){
      this.color = 'transparent';
      this.status = 'Very Poor';
    }
    if(this.current == 1){
      this.color = 'red';
      this.status = 'Poor';
    }
    if(this.current == 2){
      this.color = 'orange';
      this.status = 'Average';
    }
    if(this.current == 3){
      this.color = 'green';
      this.status = 'Good';
    } 
    // this.FinalMedalCount = this.db.removeCollection("FinalMedalCount");
    // this.FinalMedalCount = this.db.addCollection("FinalMedalCount");
    // this.FinalMedalCount.insert(TotalMedal);
    var resultSet = this.users.find( {userEmail: this.email} )[0];
    for(var i=0; i < this.users.data.length;i++){
      if(this.users.data[i].userEmail == resultSet.userEmail){
        this.users.data[i].userDailyMedal = TotalMedal;
      }
    }
    
    this.db.saveDatabase();
    console.log(this.users.data);    
  }
  gotoWeekly(){
    this.navCtrl.push("WeeklyReportPage",{userEmail: this.email});
  }

  getOverlayStyle() {
    let isSemi = this.semicircle;
    let transform = (isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

    return {
      'top': isSemi ? 'auto' : '50%',
      'bottom': isSemi ? '5%' : 'auto',
      'left': '50%',
      'transform': transform,
      '-moz-transform': transform,
      '-webkit-transform': transform,
      'font-size': this.radius / 3.5 + 'px', 
      'position': 'absolute',
      'line-height': '1'
    };
  }


}
