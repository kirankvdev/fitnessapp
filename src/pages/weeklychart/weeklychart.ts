import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Color } from 'ng2-charts';
/**
 * Generated class for the WeeklychartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-weeklychart',
  templateUrl: 'weeklychart.html',
})
export class WeeklychartPage {
public chartData;
public chartColor;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.chartData = this.navParams.get('chartData');
    this.chartColor = this.navParams.get('chartColor');
      this.barChartColors[0].backgroundColor = this.chartColor;
    console.log(this.barChartColors[0].backgroundColor)
    this.barChartData[0].label= this.chartData;
    
    console.log(this.chartData)
  }
  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  
  public barChartLabels:string[] = ['Day1', 'Day2', 'Day2', 'Day4', 'Day5', 'Day6', 'Day7'];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
  public barChartColors: Color[] = [
    { backgroundColor: 'purple' }
    
  ]

  

  public barChartData:any[] = [
    {data: [0, 1, 3, 4, 3, 2, 5], label: ''},
    ];
  
  public chartClicked(e:any):void {
    console.log(e);
    console.log("data",e.active[0])
    console.log(e.active[0]._view)
    console.log(e.active[0]._view.backgroundColor);
    if (e.active.length > 0) {
      const chart = e.active[0]._chart;
      const activePoints = chart.getElementAtEvent(e.event);
        if ( activePoints.length > 0) {
          // get the internal index of slice in pie chart
          const clickedElementIndex = activePoints[0]._index;
          const label = chart.data.labels[clickedElementIndex];
          // get value by index
          const value = chart.data.datasets[0].data[clickedElementIndex];
          console.log(clickedElementIndex, label, value)
          this.navCtrl.push('MonthlychartPage',{chartData:this.chartData, chartColor:e.active[0]._view.backgroundColor});
        }
       }
  }
  
  public chartHovered(e:any):void {
    console.log(e);
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad WeeklychartPage');
  }

}
