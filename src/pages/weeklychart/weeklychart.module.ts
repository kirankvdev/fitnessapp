import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeeklychartPage } from './weeklychart';
import { ChartsModule } from 'ng2-charts';
@NgModule({
  declarations: [
    WeeklychartPage,
  ],
  imports: [
    IonicPageModule.forChild(WeeklychartPage),
    ChartsModule
  ],
})
export class WeeklychartPageModule {}
