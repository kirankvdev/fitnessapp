import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, AlertController, ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-outlook',
  templateUrl: 'outlook.html'
})
export class OutlookPage {
  dailyOutlookScore;
  remarks;
  db;
  users;
  email;

  outlookScale=[ 
    { OptionNo: "1", Question: "Nothing can be better than my life", Score: 5, Remarks: "Excellent"}, 
    { OptionNo: "2", Question: "Life is good and I am grateful", Score: 4, Remarks: "Very Good"},
    { OptionNo: "3", Question: "I was successful in keeping the negative thoughts at bay", Score: 3, Remarks: "Good"},
    { OptionNo: "4", Question: "I have been complaining constantly", Score: 2, Remarks: "Poor"},
    { OptionNo: "5", Question: "Feeling very lonely and negative", Score: 1, Remarks: "Very Poor"}
  ];

  constructor(public navCtrl: NavController,public navParams: NavParams, public formBuilder: FormBuilder,private storage: Storage,private alertCtrl: AlertController,private viewCtrl: ViewController) {
    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter(); 
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });
    this.email = navParams.get('userEmail');
  } 
  loki(){
    this.users = this.db.getCollection("users");
  }
  outlookChanged(selectedValue: any){
    this.dailyOutlookScore = selectedValue.Score;
    this.remarks = (selectedValue.Remarks);      
  }

  NextModule(){    
    var resultSet = this.users.find( {userEmail: this.email} )[0];
    resultSet["dailyOutlookScore"] = this.dailyOutlookScore;
    this.db.saveDatabase();
    this.navCtrl.push("DietPage",{userEmail: this.email});
  }

  


}
