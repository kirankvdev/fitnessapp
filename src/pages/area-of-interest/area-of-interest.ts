import { Component} from '@angular/core';
import { NavController, MenuController, IonicPage, NavParams, AlertController } from 'ionic-angular';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-area-of-interest',
  templateUrl: 'area-of-interest.html'
})
export class AreaOfInterestPage {
  email;
  
  AreaType = ["Diet", "Fitness", "Stress", "Sleep", "Outlook"];
  constructor(public navCtrl: NavController,public menuCtrl: MenuController,public navParams: NavParams,private alertCtrl: AlertController) {
    
    this.email = navParams.get('userEmail');

    
  }
  openWorkoutSuggestion(){
    this.navCtrl.push('WorkoutSuggestionInitialPage',{userEmail: this.email});
  }
   
}
