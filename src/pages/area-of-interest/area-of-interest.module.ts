import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AreaOfInterestPage } from './area-of-interest';


@NgModule({
  declarations: [
    AreaOfInterestPage,
  ],
  imports: [
    IonicPageModule.forChild(AreaOfInterestPage)
  ],
})
export class AreaOfInterestPageModule { }