import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, AlertController, ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-diet',
  templateUrl: 'diet.html'
})
export class DietPage {
  dailyWeightedDietQ1Score;
  dailyWeightedDietQ2Score;
  dailyWeightedDietQ3Score;
  dailyWeightedDietQ4Score;
  dailyWeightedDietQ5Score;
  dailyWeightedDietQ6Score;
  dailyWeightedDietQ7Score;
  dailyWeightedDietQ8Score;
  dailyWeightedDietQ9Score;
  dailyWeightedDietQ10Score;

  answerDietQ1Score;
  answerDietQ2Score;
  answerDietQ3Score;
  answerDietQ4Score;
  answerDietQ5Score;
  answerDietQ6Score;
  answerDietQ7Score;
  answerDietQ8Score;
  answerDietQ9Score;
  answerDietQ10Score;

  answerDietScore;
  dailyDietScore;

  db;
  users;
  email;

  dietQ1Scale=[ 
    { OptionNo: "1", Question: "1-2 portion of fruit/veg", Score: 0.25},
    { OptionNo: "2", Question: "2-3 portions of fruit/veg", Score: 0.5},
    { OptionNo: "3", Question: "4 portions of fruit/veg", Score: 0.75},
    { OptionNo: "4", Question: "5+ portions", Score: 1}
  ];
  dietQ2Scale=[ 
    { OptionNo: "1", Question: "upto 20 grams", Score: 0.25},
    { OptionNo: "2", Question: "21-35 grams", Score: 0.5},
    { OptionNo: "3", Question: "26-49 grams", Score: 0.75},
    { OptionNo: "4", Question: "50grams and above", Score: 1}
  ];
  dietQ3Scale=[ 
    { OptionNo: "1", Question: "very high (Fried)", Score: 0.25},
    { OptionNo: "2", Question: "high", Score: 0.5},
    { OptionNo: "3", Question: "medium", Score: 0.75},
    { OptionNo: "4", Question: "low", Score: 1}
  ];
  dietQ4Scale=[ 
    { OptionNo: "1", Question: "very high", Score: 0.25},
    { OptionNo: "2", Question: "high", Score: 0.5},
    { OptionNo: "3", Question: "medium", Score: 0.75},
    { OptionNo: "4", Question: "low", Score: 1}
  ];
  dietQ5Scale=[ 
    { OptionNo: "1", Question: "Yes", Score: -0.5},
    { OptionNo: "2", Question: "No", Score: 0}
  ];
  dietQ6Scale=[ 
    { OptionNo: "1", Question: "Yes", Score: 1},
    { OptionNo: "2", Question: "No", Score: -1}
  ];
  dietQ7Scale=[ 
    { OptionNo: "1", Question: "Yes", Score: -0.5},
    { OptionNo: "2", Question: "No", Score: 0}
  ];
  dietQ8Scale=[ 
    { OptionNo: "1", Question: "Yes", Score: -0.5},
    { OptionNo: "2", Question: "No", Score: 0}
  ];
  dietQ9Scale=[ 
    { OptionNo: "1", Question: "Yes", Score: -1},
    { OptionNo: "2", Question: "No", Score: 0}
  ];
  dietQ10Scale=[ 
    { OptionNo: "1", Question: "Yes", Score: -1},
    { OptionNo: "2", Question: "No", Score: 0}
  ];

  dailyOutlookScore;

  constructor(public navCtrl: NavController,public navParams: NavParams, public formBuilder: FormBuilder,private storage: Storage,private alertCtrl: AlertController,private viewCtrl: ViewController) {
    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter(); 
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });
    this.email = navParams.get('userEmail');
    
  } 
  loki(){
    this.users = this.db.getCollection("users");
  }
  dietQ1Changed(selectedValue: any){
    this.dailyWeightedDietQ1Score = selectedValue.Score;
    this.answerDietQ1Score = this.dailyWeightedDietQ1Score;
  }
  dietQ2Changed(selectedValue: any){
    this.dailyWeightedDietQ2Score = selectedValue.Score;
    this.answerDietQ2Score = this.dailyWeightedDietQ2Score;
  }
  dietQ3Changed(selectedValue: any){
    this.dailyWeightedDietQ3Score = selectedValue.Score;
    this.answerDietQ3Score = this.dailyWeightedDietQ3Score;
  }
  dietQ4Changed(selectedValue: any){
    this.dailyWeightedDietQ4Score = selectedValue.Score;
    this.answerDietQ4Score = this.dailyWeightedDietQ4Score;
  }
  dietQ5Changed(selectedValue: any){
    this.dailyWeightedDietQ5Score = selectedValue.Score;
    this.answerDietQ5Score = this.dailyWeightedDietQ5Score;
  }
  dietQ6Changed(selectedValue: any){
    this.dailyWeightedDietQ6Score = selectedValue.Score;
    this.answerDietQ6Score = this.dailyWeightedDietQ6Score;
  }
  dietQ7Changed(selectedValue: any){
    this.dailyWeightedDietQ7Score = selectedValue.Score;
    this.answerDietQ7Score = this.dailyWeightedDietQ7Score;
  }
  dietQ8Changed(selectedValue: any){
    this.dailyWeightedDietQ8Score = selectedValue.Score;
    this.answerDietQ8Score = this.dailyWeightedDietQ8Score;
  }
  dietQ9Changed(selectedValue: any){
    this.dailyWeightedDietQ9Score = selectedValue.Score;
    this.answerDietQ9Score = this.dailyWeightedDietQ9Score;
  }
  dietQ10Changed(selectedValue: any){
    this.dailyWeightedDietQ10Score = selectedValue.Score;
    this.answerDietQ10Score = this.dailyWeightedDietQ10Score;
  }

  NextModule(){ 
    this.answerDietScore = this.answerDietQ1Score+this.answerDietQ2Score+this.answerDietQ3Score+this.answerDietQ4Score+this.answerDietQ5Score+this.answerDietQ6Score+this.answerDietQ7Score+this.answerDietQ8Score+this.answerDietQ9Score+this.answerDietQ10Score;

    this.dailyDietScore = (this.answerDietScore);
    var resultSet = this.users.find( {userEmail: this.email} )[0];
    resultSet["dailyDietScore"] = this.dailyDietScore;
    this.db.saveDatabase();
    this.navCtrl.push('SleepPage',{userEmail: this.email});
  }

  


}
