import { Component} from '@angular/core';
import { NavController, IonicPage, AlertController,NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-moderate',
  templateUrl: 'moderate.html'
})
export class ModeratePage { 
  db: any;                        // LokiJS database
  users: any;                  // our DB's document collection object
  reports: any;
  moderateReports: any;

  email;
  newindusModerate = [];
  activityTypeValue;
  TimePerDayValue;
  DayPerWeekValue;
  isTimeSpentSelected: boolean;
  isModerateFlag: boolean;
  iseditModerate:boolean;
  
  activityType=[ 
    { ActivityName: "walking fast", Type: "Moderate"}, 
    { ActivityName: "water aerobics", Type: "Moderate"}, 
    { ActivityName: "riding a bike on level ground or with few hills", Type: "Moderate"},
    { ActivityName: "doubles tennis", Type: "Moderate"},
    { ActivityName: "pushing a lawn mower", Type: "Moderate"},
    { ActivityName: "hiking", Type: "Moderate"},
    { ActivityName: "skateboarding", Type: "Moderate"},    
    { ActivityName: "rollerblading", Type: "Moderate"},    
    { ActivityName: "volleyball", Type: "Moderate"},    
    { ActivityName: "basketball", Type: "Moderate"},        
    { ActivityName: "going to the gym", Type: "Moderate"}
  ];
  timePerDay = ["5","10","15","20","25","30","35","40","45","50","55","60","65","70","75","80","85","90"];
  dayPerWeek = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];

  constructor(public navCtrl: NavController,private alertCtrl: AlertController,private storage: Storage,public formBuilder: FormBuilder,public navParams: NavParams) {
    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter();
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    }); 
    this.email = navParams.get('userEmail');
    // this.iseditModerate = navParams.get('iseditModerate');
    
  }
  loki(){
    this.users = this.db.getCollection("users");
    console.log(this.users);
    
    // console.log(this.moderateReports); 
  }
  selectTimePerDay(selectedValue: any,index: any){
    
    var selectedObjectCount = this.newindusModerate.length;
    this.newindusModerate = this.insertTimeItem(this.newindusModerate,index,selectedValue);
  }
  selectDayPerWeek(selectedValue: any,index: any){
    var selectedObjectCount = this.newindusModerate.length;
    this.newindusModerate = this.insertDayItem(this.newindusModerate,index,selectedValue);
  }
  updateAnswer(data,index){
      if(data.checked==true){
        var selectedActivity = {ActivityName:"",TimePerDay:"",DayPerWeek:"",IndexVal:"",Type:""};
        selectedActivity.IndexVal = index;
        selectedActivity.ActivityName = data.ActivityName;
        selectedActivity.TimePerDay = data.TimePerDayValue;
        selectedActivity.DayPerWeek = data.DayPerWeekValue;
        selectedActivity.Type = data.Type;
       this.newindusModerate.push(selectedActivity);
      } else if(data.checked==false){
        for(var i=0;i<this.newindusModerate.length;i++){
          if(data.ActivityName == this.newindusModerate[i].ActivityName){
            this.newindusModerate.splice(i,1); 
          }    
        }    
      }
  }
  
  insertTimeItem(obj,indx,val){
    for(var i=0;i<obj.length;i++){
      if(obj[i].IndexVal==indx){
        obj[i].TimePerDay = val;
      }
    }
    return obj;
  }

  insertDayItem(obj,indx,val){
    for(var i=0;i<obj.length;i++){
      if(obj[i].IndexVal==indx){
        obj[i].DayPerWeek = val;
      }
    }
    return obj;
  }

  
  openModerateActivity(){    
    var reports = {Monday:"",Tuesday: "",Wednesday: "",Thursday: "",Friday: "",Saturday:"",Sunday:""};
    var Monday = [];
    var Tuesday = [];
    var Wednesday = [];
    var Thursday = [];
    var Friday = [];
    var Saturday = [];
    var Sunday = [];
    if(this.newindusModerate.length > 0){
      for(var i=0;i<this.newindusModerate.length;i++){
          for(var j=0;j<this.newindusModerate[i].DayPerWeek.length;j++){
            if(this.newindusModerate[i].DayPerWeek[j] == "Monday")
            {       
              var MondayActivity = {ActivityName:"",Type:"",TimePerDay:""};
              MondayActivity["ActivityName"] = this.newindusModerate[i].ActivityName;
              MondayActivity["Type"] = this.newindusModerate[i].Type;
              MondayActivity["TimePerDay"] = this.newindusModerate[i].TimePerDay;
              Monday.push(MondayActivity);
            }            
            if(this.newindusModerate[i].DayPerWeek[j] == "Tuesday") 
            {       
              var TuesdayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              TuesdayActivity["ActivityName"] = this.newindusModerate[i].ActivityName;
              TuesdayActivity["Type"] = this.newindusModerate[i].Type;
              TuesdayActivity["TimePerDay"] = this.newindusModerate[i].TimePerDay; 
              Tuesday.push(TuesdayActivity); 
            }
            if(this.newindusModerate[i].DayPerWeek[j] == "Wednesday")
            {       
              var WednesdayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              WednesdayActivity["ActivityName"] = this.newindusModerate[i].ActivityName;
              WednesdayActivity["Type"] = this.newindusModerate[i].Type;
              WednesdayActivity["TimePerDay"] = this.newindusModerate[i].TimePerDay;
              Wednesday.push(WednesdayActivity);          
            }
            if(this.newindusModerate[i].DayPerWeek[j] == "Thursday")
            {       
              var ThursdayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              ThursdayActivity["ActivityName"] = this.newindusModerate[i].ActivityName;
              ThursdayActivity["Type"] = this.newindusModerate[i].Type;
              ThursdayActivity["TimePerDay"] = this.newindusModerate[i].TimePerDay;
              Thursday.push(ThursdayActivity);         
            }
            if(this.newindusModerate[i].DayPerWeek[j] == "Friday")
            {       
              var FridayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              FridayActivity["ActivityName"] = this.newindusModerate[i].ActivityName;
              FridayActivity["Type"] = this.newindusModerate[i].Type;
              FridayActivity["TimePerDay"] = this.newindusModerate[i].TimePerDay;
              Friday.push(FridayActivity);          
            }
            if(this.newindusModerate[i].DayPerWeek[j] == "Saturday")
            {       
              var SaturdayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              SaturdayActivity["ActivityName"] = this.newindusModerate[i].ActivityName;
              SaturdayActivity["Type"] = this.newindusModerate[i].Type;
              SaturdayActivity["TimePerDay"] = this.newindusModerate[i].TimePerDay;
              Saturday.push(SaturdayActivity);          
            }
            if(this.newindusModerate[i].DayPerWeek[j] == "Sunday")
            {       
              var SundayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              SundayActivity["ActivityName"] = this.newindusModerate[i].ActivityName;
              SundayActivity["Type"] = this.newindusModerate[i].Type;
              SundayActivity["TimePerDay"] = this.newindusModerate[i].TimePerDay;
              Sunday.push(SundayActivity);          
            }
          }
        console.log(this.newindusModerate);
      }
          
      reports["Monday"] = JSON.stringify(Monday);
      reports["Tuesday"] = JSON.stringify(Tuesday);
      reports["Wednesday"] = JSON.stringify(Wednesday);
      reports["Thursday"] = JSON.stringify(Thursday);
      reports["Friday"] = JSON.stringify(Friday);
      reports["Saturday"] = JSON.stringify(Saturday);
      reports["Sunday"] = JSON.stringify(Sunday);
      // console.log(reports);

        // this.moderateReports = this.db.removeCollection("moderateReports");
        // this.moderateReports = this.db.addCollection("moderateReports");        
        // this.moderateReports.insert(reports); 
        
        
          var resultSet = this.users.find( {userEmail: this.email} )[0];
          console.log(resultSet.userEmail);
          for(var i=0; i < this.users.data.length;i++){
            if(this.users.data[i].userEmail == resultSet.userEmail){
              this.users.data[i].userReports = reports;
            }
          }
          
          this.db.saveDatabase();
        
        this.navCtrl.push('WorkoutPlanPage',{userEmail: this.email});
       
  }else{
    let alert = this.alertCtrl.create({
      message: 'Please select the workout plan',
      buttons: ['Ok'] 
    });
    alert.present();
  }  
  console.log(this.users.data);
  }
}
