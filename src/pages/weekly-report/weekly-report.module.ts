import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeeklyReportPage } from './weekly-report';
import {RoundProgressModule} from 'angular-svg-round-progressbar';


@NgModule({
  declarations: [
    WeeklyReportPage,
  ],
  imports: [
    IonicPageModule.forChild(WeeklyReportPage),
       RoundProgressModule
  ],
})
export class WeeklyReportPageModule { }