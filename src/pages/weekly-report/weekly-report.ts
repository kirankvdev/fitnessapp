import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-weekly-report',
  templateUrl: 'weekly-report.html',
})
export class WeeklyReportPage {
  db: any;                        // LokiJS database
  users: any;                  // our DB's document collection object
  FinalMedalCount: any;

  email;
  isBronze: boolean;
  isSilver: boolean;
  isGold: boolean;

  current;
  max: number = 3;
  stroke: number = 5;
  radius: number = 135;
  semicircle: boolean = false;
  rounded: boolean = true;
  responsive: boolean = true;
  clockwise: boolean = true;
  color: string = '';
  background: string = '#eaeaea';
  duration: number = 5000;
  animation: string = 'easeOutCubic';
  animationDelay: number = 0;
  animations: string[] = [];
  gradient: boolean = false;
  realCurrent: number = 0;
  rate:number;
  status: string ='';
  constructor(public navCtrl: NavController, public navParams: NavParams,private storage: Storage) {
    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter();
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });
    
    // this.email = navParams.get('userEmail');
    storage.get('userdata').then((val) => {
      console.log('Your email id is', val.userEmail);
      this.email = val.userEmail;
    });
  }
  loki(){
    // this.FinalMedalCount = this.db.getCollection("FinalMedalCount"); 
    this.users = this.db.getCollection("users");
    console.log(this.users.data);
    var resultSet = this.users.find( {userEmail: this.email} )[0];
    for(var i=0;i<this.users.data.length;i++){
      if(this.users.data[i].userEmail == resultSet.userEmail){
        var Monday = this.users.data[i].userDailyMedal.Monday;      
        var Tuesday = this.users.data[i].userDailyMedal.Tuesday;
        var Wednesday = this.users.data[i].userDailyMedal.Wednesday;
        var Thursday = this.users.data[i].userDailyMedal.Thursday;
        var Friday = this.users.data[i].userDailyMedal.Friday;
        var Saturday = this.users.data[i].userDailyMedal.Saturday;
        var Sunday = this.users.data[i].userDailyMedal.Sunday;
      }
    }
    var score = Monday + Tuesday + Wednesday + Thursday + Friday + Saturday + Sunday;
    if(score == 0){
      console.log("No Workout Has Been done");
    }else{
      if(score > 1 && score <= 4){
        this.isBronze = true;
        this.current = 1;
      }else if(score >= 5 && score < 9){
        this.isSilver = true;
        this.current = 2;
      }else if(score >= 9){
        this.isGold = true;
        this.current = 3;
      }
    }
      
    if(this.current == 1){
      this.color = 'red';
      this.status = 'Poor';
    }
    if(this.current == 2){ 
      this.color = 'orange';
      this.status = 'Average';
    }
    if(this.current == 3){
      this.color = 'green';
      this.status = 'Good'; 
    }
  }
  

  getOverlayStyle() {
    let isSemi = this.semicircle;
    let transform = (isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

    return {
      'top': isSemi ? 'auto' : '50%',
      'bottom': isSemi ? '5%' : 'auto',
      'left': '50%',
      'transform': transform,
      '-moz-transform': transform,
      '-webkit-transform': transform,
      'font-size': this.radius / 3.5 + 'px', 
      'position': 'absolute',
      'line-height': '1'
    };
  }


}
