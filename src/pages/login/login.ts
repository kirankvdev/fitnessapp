import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, AlertController, ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  db: any;                        // LokiJS database
  users: any;                  // our DB's document collection object

  results;
  getResult;
  email;
  password;
  loginFormData: FormGroup;

  constructor(public navCtrl: NavController,public navParams: NavParams, public formBuilder: FormBuilder,private storage: Storage,private alertCtrl: AlertController,private viewCtrl: ViewController) {
    let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.loginFormData = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.pattern(EMAILPATTERN)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(12)])
    });

    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter();
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });
  }  
  ionViewWillEnter() {
      this.viewCtrl.showBackButton(false);
  }
  loki(){
    this.users = this.db.getCollection("users");
    if(!this.users){
      this.users = this.db.addCollection("users", {
        indices: ['location']
      });      
    } 
  }
  login(){   
    console.log(this.users.data);   
      if(this.users.data.length == 0){
        let alert = this.alertCtrl.create({
          title: 'Please Check ',
          message: "The user doesn't exist",
          buttons: ['Ok'] 
        });
        alert.present();
      }
      else{
        var getIndex = this.users.find( {userEmail: this.email} );
        if(getIndex.length == 1){
          if(getIndex[0].userEmail == this.email && getIndex[0].userPassword == this.password){
            this.storage.set('userdata', {'userEmail': this.email});
            if(!(getIndex[0].userFirstTime)){
              this.navCtrl.setRoot('DashboardPage',{userEmail: this.email,isHideBackButton: true});
            }else{              
            this.navCtrl.push('PersonalDetailsPage',{userEmail: this.email, userFirstTime: true}); 
            }
          }else if (getIndex[0].userEmail != this.email || getIndex[0].userPassword != this.password){
            let alert = this.alertCtrl.create({
              title: 'Please Check ',
              message: "The email and password is incorrect",
              buttons: ['Ok'] 
            });
            alert.present();
          }
        }else{
          let alert = this.alertCtrl.create({
            title: 'Sorry ',
            message: "The email and password doesn't exist",
            buttons: ['Ok']
          });
          alert.present();
        }
      }
      
  }
  
  signup(){
    this.navCtrl.push('SignupPage');
  }
  forgotPassword(){
    this.navCtrl.push('ForgotPasswordPage');
  }

  public type = 'password';
  public showPass = false;


  showPassword() {
    this.showPass = !this.showPass;

    if(this.showPass){
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }
}
