import { Component} from '@angular/core';
import { NavController, IonicPage, NavParams, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  db: any;                        // LokiJS database
  users: any;                  // our DB's document collection object

  fullName: string;
  email;
  password;
  userData;
  signUpFormData: FormGroup;
  constructor(public navCtrl: NavController,public navParams: NavParams, public formBuilder: FormBuilder,private storage: Storage,private alertCtrl: AlertController) {
    let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.signUpFormData = this.formBuilder.group({
      fullName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.pattern(EMAILPATTERN)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(12)])
    });

    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter();
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });
  }
  createAccount(){
    var getIndex = this.users.find( {userEmail: this.email} );
    if(getIndex.length == 0){
      this.users.insert(
          {
            "userName": this.fullName,
            "userEmail": this.email,
            "userPassword": this.password,
            "userReports": "",
            "userFirstTime": true,
          }
        );
        this.navCtrl.push('LoginPage');
    }else if(getIndex.length >= 1){
      let alert = this.alertCtrl.create({
        message: "The Email ID you have entered already exists",
        buttons: ['Ok']
      });
      alert.present();
    }   
  }
  loki(){
    
    this.users = this.db.getCollection("users");
    if(!this.users){
      this.users = this.db.addCollection("users", {
        indices: ['location']
      });      
    }
  }
  login(){
    this.navCtrl.push('LoginPage');
  }
  public type = 'password';
  public showPass = false;
 
 
  showPassword() {
    this.showPass = !this.showPass;
 
    if(this.showPass){
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }
}
