import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, AlertController, ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-stress',
  templateUrl: 'stress.html'
})
export class StressPage {
  dailyStressScore;
  remarks;

  db;
  users;
  email;

  stressScale=[ 
    { OptionNo: "1", Question: "A good day", Score: 5, Remarks: "Excellent"}, 
    { OptionNo: "2", Question: "I was feeling a bit low and uninterested but all good", Score: 4, Remarks: "Very Good"},
    { OptionNo: "3", Question: "I was undenr pressure but overcame and feel good now", Score: 3, Remarks: "Good"},
    { OptionNo: "4", Question: "I feel Panicked and anxious sometimes", Score: 2, Remarks: "Poor"},
    { OptionNo: "5", Question: "Very Anxious most of the day", Score: 1, Remarks: "Very Poor"}
  ];
  
  dailyOutlookScore;
  dailyDietScore;
  dailySleepScore;

  constructor(public navCtrl: NavController,public navParams: NavParams, public formBuilder: FormBuilder,private storage: Storage,private alertCtrl: AlertController,private viewCtrl: ViewController) {
    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter(); 
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });
    this.email = navParams.get('userEmail');
  } 
  loki(){
    this.users = this.db.getCollection("users");
  }
  stressChanged(selectedValue: any){
    this.dailyStressScore = selectedValue.Score;
    this.remarks = (selectedValue.Remarks);
  }

  NextModule(){
    var resultSet = this.users.find( {userEmail: this.email} )[0];
    resultSet["dailyStressScore"] = this.dailyStressScore;
    this.db.saveDatabase();
    this.navCtrl.push("DailychartPage",{userEmail: this.email});
  }

  


}
