import { Component} from '@angular/core';
import { NavController, IonicPage, AlertController,NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-vigorous',
  templateUrl: 'vigorous.html'
})
export class VigorousPage {
  db: any;                        // LokiJS database
  users: any;                  // our DB's document collection object
  reports: any;
  vigorousReports: any;

  email;
  newindusVigorous = [];
  activityTypeValue;
  TimePerDayValue;
  DayPerWeekValue;
  isTimeSpentSelected: boolean;
  isVigorousFlag:boolean; 
  iseditVigorous:boolean;  
  
  activityType=[ 
    { ActivityName: "jogging or running", Type: "Vigorous"}, 
    { ActivityName: "swimming fast", Type: "Vigorous"}, 
    { ActivityName: "riding a bike fast or on hills", Type: "Vigorous"},
    { ActivityName: "singles tennis", Type: "Vigorous"},
    { ActivityName: "football", Type: "Vigorous"},
    { ActivityName: "rugby", Type: "Vigorous"},
    { ActivityName: "skipping rope", Type: "Vigorous"},    
    { ActivityName: "hockey", Type: "Vigorous"},    
    { ActivityName: "aerobics", Type: "Vigorous"},    
    { ActivityName: "gymnastics", Type: "Vigorous"},        
    { ActivityName: "martial arts", Type: "Vigorous"}   
  ];
  timePerDay = ["5","10","15","20","25","30","35","40","45","50","55","60","65","70","75","80","85","90"];
  dayPerWeek = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
  
  constructor(public navCtrl: NavController,private alertCtrl: AlertController,private storage: Storage,public navParams: NavParams) {
    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter();
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });   
    this.email = navParams.get('userEmail');
    // this.iseditVigorous = navParams.get('iseditVigorous');
  }
  loki(){
    this.users = this.db.getCollection("users"); 
    console.log(this.users);
  }
  selectTimePerDay(selectedValue: any,index: any){
    
    var selectedObjectCount = this.newindusVigorous.length;
    this.newindusVigorous = this.insertTimeItem(this.newindusVigorous,index,selectedValue);
  }
  selectDayPerWeek(selectedValue: any,index: any){
    var selectedObjectCount = this.newindusVigorous.length;
    this.newindusVigorous = this.insertDayItem(this.newindusVigorous,index,selectedValue);
  }
  updateAnswer(data,index){
      if(data.checked==true){
        var selectedActivity = {ActivityName:"",TimePerDay:"",DayPerWeek:"",IndexVal:"",Type:""};
        selectedActivity.IndexVal = index;
        selectedActivity.ActivityName = data.ActivityName;
        selectedActivity.TimePerDay = data.TimePerDayValue;
        selectedActivity.DayPerWeek = data.DayPerWeekValue;
        selectedActivity.Type = data.Type;
       this.newindusVigorous.push(selectedActivity); 
      } else if(data.checked==false){
        for(var i=0;i<this.newindusVigorous.length;i++){
          if(data.ActivityName == this.newindusVigorous[i].ActivityName){
            this.newindusVigorous.splice(i,1); 
          }    
        }    
      }
  }
  
  insertTimeItem(obj,indx,val){
    for(var i=0;i<obj.length;i++){
      if(obj[i].IndexVal==indx){
        obj[i].TimePerDay = val;
      }
    }
    return obj;
  }

  insertDayItem(obj,indx,val){
    for(var i=0;i<obj.length;i++){
      if(obj[i].IndexVal==indx){
        obj[i].DayPerWeek = val;
      }
    }
    return obj;
  }

  
  openVigorousActivity(){    
    var reports = {Monday:"",Tuesday: "",Wednesday: "",Thursday: "",Friday: "",Saturday:"",Sunday:""};
    var Monday = [];
    var Tuesday = [];
    var Wednesday = [];
    var Thursday = [];
    var Friday = [];
    var Saturday = [];
    var Sunday = [];
    if(this.newindusVigorous.length > 0){
      for(var i=0;i<this.newindusVigorous.length;i++){
          for(var j=0;j<this.newindusVigorous[i].DayPerWeek.length;j++){
            if(this.newindusVigorous[i].DayPerWeek[j] == "Monday")
            {       
              var MondayActivity = {ActivityName:"",Type:"",TimePerDay:""};
              MondayActivity["ActivityName"] = this.newindusVigorous[i].ActivityName;
              MondayActivity["Type"] = this.newindusVigorous[i].Type;
              MondayActivity["TimePerDay"] = this.newindusVigorous[i].TimePerDay;
              Monday.push(MondayActivity);
            }
            if(this.newindusVigorous[i].DayPerWeek[j] == "Tuesday") 
            {       
              var TuesdayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              TuesdayActivity["ActivityName"] = this.newindusVigorous[i].ActivityName;
              TuesdayActivity["Type"] = this.newindusVigorous[i].Type;
              TuesdayActivity["TimePerDay"] = this.newindusVigorous[i].TimePerDay; 
              Tuesday.push(TuesdayActivity); 
            }
            if(this.newindusVigorous[i].DayPerWeek[j] == "Wednesday")
            {       
              var WednesdayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              WednesdayActivity["ActivityName"] = this.newindusVigorous[i].ActivityName;
              WednesdayActivity["Type"] = this.newindusVigorous[i].Type;
              WednesdayActivity["TimePerDay"] = this.newindusVigorous[i].TimePerDay;
              Wednesday.push(WednesdayActivity);          
            }
            if(this.newindusVigorous[i].DayPerWeek[j] == "Thursday")
            {       
              var ThursdayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              ThursdayActivity["ActivityName"] = this.newindusVigorous[i].ActivityName;
              ThursdayActivity["Type"] = this.newindusVigorous[i].Type;
              ThursdayActivity["TimePerDay"] = this.newindusVigorous[i].TimePerDay;
              Thursday.push(ThursdayActivity);         
            }
            if(this.newindusVigorous[i].DayPerWeek[j] == "Friday")
            {       
              var FridayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              FridayActivity["ActivityName"] = this.newindusVigorous[i].ActivityName;
              FridayActivity["Type"] = this.newindusVigorous[i].Type;
              FridayActivity["TimePerDay"] = this.newindusVigorous[i].TimePerDay;
              Friday.push(FridayActivity);          
            }
            if(this.newindusVigorous[i].DayPerWeek[j] == "Saturday")
            {       
              var SaturdayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              SaturdayActivity["ActivityName"] = this.newindusVigorous[i].ActivityName;
              SaturdayActivity["Type"] = this.newindusVigorous[i].Type;
              SaturdayActivity["TimePerDay"] = this.newindusVigorous[i].TimePerDay;
              Saturday.push(SaturdayActivity);          
            }
            if(this.newindusVigorous[i].DayPerWeek[j] == "Sunday")
            {       
              var SundayActivity = {ActivityName:"",Type:"",TimePerDay:""};   
              SundayActivity["ActivityName"] = this.newindusVigorous[i].ActivityName;
              SundayActivity["Type"] = this.newindusVigorous[i].Type;
              SundayActivity["TimePerDay"] = this.newindusVigorous[i].TimePerDay;
              Sunday.push(SundayActivity);          
            }
          }
        
      }     
      reports["Monday"] = JSON.stringify(Monday);
      reports["Tuesday"] = JSON.stringify(Tuesday);
      reports["Wednesday"] = JSON.stringify(Wednesday);
      reports["Thursday"] = JSON.stringify(Thursday);
      reports["Friday"] = JSON.stringify(Friday);
      reports["Saturday"] = JSON.stringify(Saturday);
      reports["Sunday"] = JSON.stringify(Sunday);
      // console.log(reports)
        // this.vigorousReports = this.db.removeCollection("vigorousReports");
        // this.vigorousReports = this.db.addCollection("vigorousReports");
        // this.vigorousReports.insert(reports); 
        var resultSet = this.users.find( {userEmail: this.email} )[0];
          console.log(resultSet.userEmail);
          for(var i=0; i < this.users.data.length;i++){
            if(this.users.data[i].userEmail == resultSet.userEmail){
              this.users.data[i].userReports = reports;
            }
          }
          
          this.db.saveDatabase();
        
        this.navCtrl.push('WorkoutPlanPage',{userEmail: this.email});
  }else{
    let alert = this.alertCtrl.create({
      message: 'Please select the workout plan',
      buttons: ['Ok'] 
    });
    alert.present();
  }
  console.log(this.users.data); 
  }
}
