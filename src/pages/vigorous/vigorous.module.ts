import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VigorousPage } from './vigorous';


@NgModule({
  declarations: [
    VigorousPage,
  ],
  imports: [
    IonicPageModule.forChild(VigorousPage)
  ],
})
export class VigorousPageModule { }