import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MonthlychartPage } from './monthlychart';
import { ChartsModule } from 'ng2-charts';
@NgModule({
  declarations: [
    MonthlychartPage,
  ],
  imports: [
    IonicPageModule.forChild(MonthlychartPage),
    ChartsModule
  ],
})
export class MonthlychartPageModule {}
