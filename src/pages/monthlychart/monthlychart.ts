import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, App } from 'ionic-angular';
import { Color } from 'ng2-charts';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-monthlychart',
  templateUrl: 'monthlychart.html',
})
export class MonthlychartPage {
  public chartData;
  public chartColor;

  db;
  users;
  email;
    constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController,public app: App) {
      this.chartData = this.navParams.get('chartData');
      this.chartColor = this.navParams.get('chartColor');
        this.barChartColors[0].backgroundColor = this.chartColor;
      console.log(this.barChartColors[0].backgroundColor)
      this.barChartData[0].label= this.chartData;
      this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter(); 
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });
    this.email = navParams.get('userEmail');
  
  }
  
  loki(){
    this.users = this.db.getCollection("users");
  }  
  logout(){
    this.menuCtrl.close();
      var nav = this.app.getRootNavs();
      nav[0].setRoot('LoginPage');
    //this.navCtrl.push("DashboardPage",{userEmail: this.email});
  }
  public barChartLabels:string[] = ['Day1', 'Day2', 'Day3', 'Day4', 'Day5', 'Day6', 'Day7', 'Day8', 'Day9', 'Day10', 'Day11', 'Day12', 'Day13', 'Day14', 'Day15', 'Day16', 'Day17', 'Day18', 'Day19', 'Day20', 'Day21', 'Day22', 'Day23', 'Day24', 'Day25', 'Day26', 'Day27', 'Day28', 'Day29', 'Day30', 'Day31'];
  public barChartType:string = 'line';
  public barChartLegend:boolean = true;
  public barChartColors: Color[] = [
    { backgroundColor: 'purple' }
    
  ]
  public barChartData:any[] = [
    {data: [1, 2, 3, 4, 3, 2, 5,1, 2, 3, 4, 3, 2, 5,1, 2, 3, 4, 3, 2, 5,1, 2, 3, 4, 3, 2, 5, 2, 2, 5], label: ''}
  ];
  public chartClicked(e:any):void {
    console.log(e);
  }
  
  public chartHovered(e:any):void {
    console.log(e);
  }
}
