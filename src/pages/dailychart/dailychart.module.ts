import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DailychartPage } from './dailychart';
import { ChartsModule } from 'ng2-charts'; // <- HERE
import {RoundProgressModule} from 'angular-svg-round-progressbar';

@NgModule({
  declarations: [
    DailychartPage,
  ],
  imports: [
    IonicPageModule.forChild(DailychartPage),
    ChartsModule,
    RoundProgressModule 
  ],
})
export class DailychartPageModule {}
