import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-dailychart',
  templateUrl: 'dailychart.html',
})
export class DailychartPage {
  dailyOutlookScore;
  dailyDietScore;
  dailySleepScore;
  dailyStressScore;
  dailyExcerciseScore;

  WeightedDailyOutlookScore;
  WeightedDailyDietScore;
  WeightedDailySleepScore;
  WeightedDailyExcerciseScore;
  WeightedDailyStressScore;

  db;
  users;
  email;

  DailyPeaceQscore;

  current;
  max: number = 5;
  stroke: number = 5;
  radius: number = 135;
  semicircle: boolean = false;
  rounded: boolean = true;
  responsive: boolean = true;
  clockwise: boolean = true;
  color: string = '';
  background: string = '#eaeaea';
  duration: number = 5000;
  animation: string = 'easeOutCubic';
  animationDelay: number = 0;
  animations: string[] = [];
  gradient: boolean = false;
  realCurrent: number = 0;
  rate:number;
  DailyPeaceQscoreText:string = '';
  // radar, pie, line, bar, polarArea, doughnut, bubble, scatter, horizontalBar
  constructor(public navCtrl: NavController,public navParams: NavParams) {
    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter(); 
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });
    this.email = navParams.get('userEmail');
  }
  

  public doughnutChartLabels:string[] = ['Positive Outlook', 'Diet', 'Sleep','Excercise','Stress'];
  public doughnutChartData:number[] = [];
  public doughnutChartType:string = 'doughnut';
  private doughnutChartColor=[
    {
      backgroundColor: [
        'rgba(255, 161, 181, 1)',
        'rgba(134, 199, 243, 1)',
        'rgba(255, 225, 153, 1)',
        'rgba(240, 242, 244, 1)',
        'rgba(147, 217, 217, 1)'
    ]
    }
  ];

loki(){
  this.users = this.db.getCollection("users");
  var resultSet = this.users.find( {userEmail: this.email} )[0];
    for(var i=0;i<this.users.data.length;i++){
      if(this.users.data[i].userEmail == resultSet.userEmail){
        this.dailyOutlookScore = JSON.parse(this.users.data[i].dailyOutlookScore);
        this.dailyDietScore = JSON.parse(this.users.data[i].dailyDietScore);
        this.dailySleepScore = JSON.parse(this.users.data[i].dailySleepScore);
        this.dailyExcerciseScore = 4;
        this.dailyStressScore = JSON.parse(this.users.data[i].dailyStressScore);
      }
    }
    var finalchartdata =  [];
    finalchartdata[0] = this.dailyOutlookScore;
    finalchartdata[1] = this.dailyDietScore;
    finalchartdata[2] = this.dailySleepScore;
    finalchartdata[3] = this.dailyExcerciseScore;
    finalchartdata[4] = this.dailyStressScore;
    this.doughnutChartData = finalchartdata;
  
    this.WeightedDailyOutlookScore = this.dailyOutlookScore  * 0.19;
    this.WeightedDailyDietScore = this.dailyDietScore * 0.17;
    this.WeightedDailySleepScore = this.dailySleepScore * 0.21;
    this.WeightedDailyExcerciseScore = this.dailyExcerciseScore * 0.21;
    this.WeightedDailyStressScore = this.dailyStressScore * 0.23;
  
    this.current = this.WeightedDailyOutlookScore + this.WeightedDailyDietScore + this.WeightedDailySleepScore + this.WeightedDailyExcerciseScore + this.WeightedDailyStressScore;
    
    this.DailyPeaceQscore = this.current;
  
    if(this.current <= 1 || this.current <=2 ){
      this.color = 'red';
    }
    if(this.current > 2 || this.current <= 3){ 
      this.color = 'orange';
    }
    if(this.current > 3 || this.current <=4){
      this.color = 'yellow';
    }
    if(this.current > 4 || this.current <=5){
      this.color = 'green';
    }
    
  
    this.DailyPeaceQscoreText = "Collabarative Score is " + this.DailyPeaceQscore;
    resultSet["SubmittedWorkout"] = true;
    resultSet["DailyPeaceQscore"] = this.current;
    this.db.saveDatabase();
   
}
getOverlayStyle() { 

  let isSemi = this.semicircle;
  let transform = (isSemi ? '' : 'translateY(80%) ') + 'translateX(-50%)';

  return {
    'top': isSemi ? 'auto' : '80%',
    'bottom': isSemi ? '5%' : 'auto',
    'left': '50%',
    'transform': transform,
    '-moz-transform': transform,
    '-webkit-transform': transform,
    'font-size': this.radius / 3.5 + 'px', 
    'position': 'absolute',
    'line-height': '1'
  };
}

// this.doughnutChartData.push(this.dailyOutlookScore);
// this.doughnutChartData.push(this.dailyDietScore);
// this.doughnutChartData.push(this.dailySleepScore);
// this.doughnutChartData.push(5);
// this.doughnutChartData.push(this.dailyStressScore);

public chartClicked(e:any):void {
  console.log(e);
  console.log("data",e.active[0])
  console.log(e.active[0]._view)
  console.log(e.active[0]._view.backgroundColor);
  if (e.active.length > 0) {
    const chart = e.active[0]._chart;
    const activePoints = chart.getElementAtEvent(e.event);
      if ( activePoints.length > 0) {
        // get the internal index of slice in pie chart
        const clickedElementIndex = activePoints[0]._index;
        const label = chart.data.labels[clickedElementIndex];
        // get value by index
        const value = chart.data.datasets[0].data[clickedElementIndex];
        console.log(clickedElementIndex, label, value)
        this.navCtrl.push('WeeklychartPage',{chartData:label, chartColor:e.active[0]._view.backgroundColor});
      }
     }
 
}

public chartHovered(e:any):void {
  console.log(e);
}

}
