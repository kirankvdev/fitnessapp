import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, AlertController, ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';

import Loki from 'lokijs';
import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
import { IndexedStorage } from '@lokidb/indexed-storage';

@IonicPage()
@Component({
  selector: 'page-sleep',
  templateUrl: 'sleep.html'
})
export class SleepPage {
  dailySleepScore;
  remarks;
  dailyOutlookScore;
  dailyDietScore;

  db;
  users;
  email;

  sleepScale=[ 
    { OptionNo: "1", Question: "6 to 8 hours consistent", Score: 5, Remarks: "Excellent"}, 
    { OptionNo: "2", Question: "6 to 8 hours inconsistent", Score: 4, Remarks: "Very Good"},
    { OptionNo: "3", Question: "6 to 8 hours disturbed", Score: 3, Remarks: "Good"},
    { OptionNo: "4", Question: "less than 6 hours", Score: 2, Remarks: "Poor"},
    { OptionNo: "5", Question: "more than 10 hours", Score: 1, Remarks: "Very Poor"}
  ];
  

  constructor(public navCtrl: NavController,public navParams: NavParams, public formBuilder: FormBuilder,private storage: Storage,private alertCtrl: AlertController,private viewCtrl: ViewController) {
    this.loki = this.loki.bind(this);
    var idbAdapter = new LokiIndexedAdapter(); 
    this.db = new Loki("fitness.db", {
      env: `BROWSER`,
      adapter: idbAdapter,
      autoload: true,
      autoloadCallback : this.loki,
      autosave: true,
      autosaveInterval: 100
    });
    this.email = navParams.get('userEmail');    
  } 
  loki(){
    this.users = this.db.getCollection("users");
  }
  sleepChanged(selectedValue: any){
    this.dailySleepScore = selectedValue.Score;
    this.remarks = (selectedValue.Remarks);
  }

  NextModule(){
    var resultSet = this.users.find( {userEmail: this.email} )[0];
    resultSet["dailySleepScore"] = this.dailySleepScore;
    this.db.saveDatabase();
    this.navCtrl.push('StressPage',{userEmail: this.email});
  }
  
  

  


}
