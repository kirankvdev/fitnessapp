import { Component } from '@angular/core';
import { NavController, IonicPage, NavParams, AlertController, Platform } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { LocalNotifications } from '@ionic-native/local-notifications';

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html'
})
export class ForgotPasswordPage {
  email;
  forgotPasswordFormData: FormGroup;
  constructor(public navCtrl: NavController,public navParams: NavParams, public formBuilder: FormBuilder,private alertCtrl: AlertController,private localNotifications: LocalNotifications,private plt: Platform) {
    let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.forgotPasswordFormData = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.pattern(EMAILPATTERN)])
    });
    this.localNotifications.schedule({
      id: 1,
      title: "Fitness App",
      text: "Have you filled your daily workout form",
      trigger: {at: new Date(new Date().getTime() + 5 * 1000)}
    });
  }
  forgotPassword(){
    let alert = this.alertCtrl.create({
      title: 'Success',
      message: "We've sent you a mail to your registered email id, please click on the link for reset password",
      buttons: ['Ok']
    });
    alert.present();
    this.navCtrl.push('LoginPage');
  }
  scheduleNotification(){
    
  }
}
